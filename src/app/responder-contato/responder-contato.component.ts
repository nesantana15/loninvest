import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-responder-contato',
  templateUrl: './responder-contato.component.html',
  styleUrls: ['./responder-contato.component.css']
})
export class ResponderContatoComponent implements OnInit {
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService, private route: ActivatedRoute) { }
  
  id_contato;
  contato;
  
  usuario;
  
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    
    this.route.params.subscribe(params => {
      this.id_contato = params['id_contato'];
    });
    
  }
  
  
  mensagemCriou() {
    this.messageService.add({severity:'success', summary: 'Contato', detail: ' respondido com sucesso'});
  }
  
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  
  enviar(form){
    this.padroes.requestPOST('/'+(this.usuario.tipo == 3 ? 'cliente' : (this.usuario.tipo == 2 ? 'consultor' : 'administrador'))+'/responder-contato/'+this.id_contato, form, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemCriou();
      this.router.navigate(['/'+(this.usuario.tipo == 3 ? 'client' : (this.usuario.tipo == 2 ? 'consultant' : 'manager'))+'/contact']);
    });
  }

}
