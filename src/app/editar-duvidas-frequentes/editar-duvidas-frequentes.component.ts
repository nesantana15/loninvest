import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-editar-duvidas-frequentes',
  templateUrl: './editar-duvidas-frequentes.component.html',
  styleUrls: ['./editar-duvidas-frequentes.component.css']
})
export class EditarDuvidasFrequentesComponent implements OnInit {
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService, private route: ActivatedRoute) { }
  
  id_duvida;
  duvida: any = {};
  
  usuario;
  
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.route.params.subscribe(params => {
      this.id_duvida = params['id_duvida'];
    });
    
    this.padroes.requestGET('/administrador/editar-duvida-frequente?duvida_id='+this.id_duvida, (response) => {
      if (response.error) {
        return;
      }
      this.duvida = response;
    });
    
  }
  
  
  mensagemCriou() {
    this.messageService.add({severity:'success', summary: 'Dúvida editada', detail: 'com sucesso'});
  }
  
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  
  enviar(form){
      this.padroes.requestPOST('/administrador/editar-duvida-frequente/'+this.id_duvida, form, (response) => {
        if (response.error) {
          this.mensagemErroInesperado();
          return;
        }
        this.mensagemCriou();
        this.router.navigate(['/manager/questions']);
      });
  }

}
