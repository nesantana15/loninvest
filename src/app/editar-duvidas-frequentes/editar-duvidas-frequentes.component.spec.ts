import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarDuvidasFrequentesComponent } from './editar-duvidas-frequentes.component';

describe('EditarDuvidasFrequentesComponent', () => {
  let component: EditarDuvidasFrequentesComponent;
  let fixture: ComponentFixture<EditarDuvidasFrequentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarDuvidasFrequentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarDuvidasFrequentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
