import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-administradores',
  templateUrl: './admin-administradores.component.html',
  styleUrls: ['./admin-administradores.component.css']
})
export class AdminAdministradoresComponent implements OnInit {

  index: number = -1;
  aportes: Array<any> = [];

  ordens: SelectItem[];
  ordem;
  busca;

  total_rendimentos = 0;
  total_aportes = 0;
  total_administradores = 0;

  administradores;

  ngOnInit() {
    this.padroes.requestGET('/administrador/administradores', (response) => {
      if (response.error) {
        return;
      }
      console.log(response);
      this.administradores = response;
      this.administradores.map((item) => {
        this.total_administradores++;
        this.total_aportes = this.total_aportes + item.valor_aportes;
        this.total_rendimentos = this.total_rendimentos + item.valor_rendimentos;
        switch (item.genero) {
          case 'masculino':
            item.genero = (this.usuario.pais == 1 ? 'Male' : 'Masculino');
            break;
          case 'feminino':
            item.genero = (this.usuario.pais == 1 ? 'Female' : 'Feminino');
            break;
          case 'homosexual':
            item.genero = 'Homosexual';
            break;
          case 'transsexual':
            item.genero = 'Transsexual';
            break;
          case 'outro':
            item.genero = (this.usuario.pais == 1 ? 'Other' : 'Outro');
            break;
        }
        switch (item.estado_civil) {
          case 'casado':
            item.estado_civil = (this.usuario.pais == 1 ? 'Married' : 'Casado');
            break;
          case 'solteiro':
            item.estado_civil = (this.usuario.pais == 1 ? 'Unmarried' : 'Solteiro');
            break;
          case 'divorciado':
            item.estado_civil = (this.usuario.pais == 1 ? 'Divorced' : 'Divorciado');
            break;
          case 'viuvo':
            item.estado_civil = (this.usuario.pais == 1 ? 'Widower' : 'Viúvo');
            break;
          case 'outro':
            item.estado_civil = (this.usuario.pais == 1 ? 'Other' : 'Outro');
            break;
        }
        switch (item.moeda) {
          case 1:
            item.moeda = 'Libra';
            break;
          case 2:
            item.moeda = 'Real';
            break;
        }
        switch (item.pais) {
          case 1:
            item.pais = (this.usuario.pais == 1 ? 'England' : 'Inglaterra');
            break;
          case 2:
            item.pais = (this.usuario.pais == 1 ? 'Brazil' : 'Brasil');
            break;
        }
      });
    });
  }
  usuario: any = {};

  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    this.usuario = this.padroes.verUsuario().usuario;
    this.ordens = [
      {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
      {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
    ];
  }

  input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
  fechar_busca_style = {'display': 'none'};

  abrir_busca() {
    this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
    this.fechar_busca_style = {'display': 'block'};
  }

  fechar_busca() {
    this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    this.fechar_busca_style = {'display': 'none'};
  }

  ordena() {
    this.padroes.requestGET('/administrador/administradores?' + this.ordem.tipo + '=' + this.ordem.name, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.administradores = response;

      console.log(this.administradores);
    });
  }

  buscar() {
    this.padroes.requestGET('/administrador/administradores?busca=' + this.busca, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.administradores = response;

      console.log(this.administradores);
    });
  }
  
  mensagemAtivarAdministrador() {
    this.messageService.add({severity:'success', summary: 'Administrador ativado', detail: 'com sucesso'});
  }
  mensagemDesativarAdministrador() {
    this.messageService.add({severity:'success', summary: 'Administrador desativado', detail: 'com sucesso'});
  }
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  
  style_certeza: any = {'opacity': '0', 'pointer-events': 'none'};
  editar_item: any = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
  certeza_abre(id_item, index_item, comprovante_id, funcao){
    this.style_certeza = {'opacity': '1', 'pointer-events': 'auto'};
    this.editar_item = {id_item: id_item, index_item: index_item, comprovante_id: comprovante_id, funcao: funcao};
  }
  cancela_certeza(){
    this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
    this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
  };
  
  desativar_usuario(usuario_id, index){
    this.padroes.requestGET('/administrador/desativar-administrador/?administrador_id='+usuario_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemDesativarAdministrador();
      this.administradores[index].ativo = 0;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }

  ativar_usuario(usuario_id, index){
    this.padroes.requestGET('/administrador/ativar-administrador/?administrador_id='+usuario_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemAtivarAdministrador();
      this.administradores[index].ativo = 1;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }
}
