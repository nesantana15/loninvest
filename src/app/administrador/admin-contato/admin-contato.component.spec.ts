import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminContatoComponent } from './admin-contato.component';

describe('AdminContatoComponent', () => {
  let component: AdminContatoComponent;
  let fixture: ComponentFixture<AdminContatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminContatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminContatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
