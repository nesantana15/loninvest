import {Component, OnInit} from '@angular/core';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
    selector: 'app-admin-novo-aporte',
    templateUrl: './admin-novo-aporte.component.html',
    styleUrls: ['./admin-novo-aporte.component.css']
})
export class AdminNovoAporteComponent implements OnInit {

    clientes: Array<any> = [];
    consultores: Array<any> = [];
    franqueados: Array<any> = [];

    consultor_do_cliente = '';
    franqueado_do_cliente = '';
    
    usuario;
    
    ngOnInit() {
        this.usuario = this.padroes.verUsuario().usuario;
        this.padroes.requestGET('/administrador/novo-aporte', (response) => {
            if (response.error) {
                return;
            }
            this.clientes = response.clientes;
            this.consultores = response.consultores;
            this.franqueados = response.franqueados;
        });
    }

    trocou_cliente(cliente){
        this.clientes.map((item) => {
            if(item.id == cliente){
                this.consultor_do_cliente = item.cliente_consultor;
                this.franqueado_do_cliente = item.cliente_franqueado;
                console.log(item, 'consultor aqui');
            }
        });
    }

    mensagemCriou() {
        this.messageService.add({severity:'success', summary: 'Aporte criado', detail: 'com sucesso'});
    }
    
    mensagemErroInesperado() {
        this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
    }
    constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    }

    criar_aporte(form){
        this.padroes.requestPOST('/administrador/inserir-aporte', form, (response) => {
            if (response.error) {
                this.mensagemErroInesperado();
                return console.log('erro');
            }
            this.mensagemCriou();
            this.router.navigate(['/manager/new-applications']);
        });
    }

}
