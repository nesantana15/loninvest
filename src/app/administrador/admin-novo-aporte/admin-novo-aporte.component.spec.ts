import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNovoAporteComponent } from './admin-novo-aporte.component';

describe('AdminNovoAporteComponent', () => {
  let component: AdminNovoAporteComponent;
  let fixture: ComponentFixture<AdminNovoAporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNovoAporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNovoAporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
