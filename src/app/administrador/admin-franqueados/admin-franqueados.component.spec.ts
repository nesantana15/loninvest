import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFranqueadosComponent } from './admin-franqueados.component';

describe('AdminFranqueadosComponent', () => {
  let component: AdminFranqueadosComponent;
  let fixture: ComponentFixture<AdminFranqueadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFranqueadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFranqueadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
