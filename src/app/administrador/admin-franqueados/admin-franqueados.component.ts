import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-franqueados',
  templateUrl: './admin-franqueados.component.html',
  styleUrls: ['./admin-franqueados.component.css']
})
export class AdminFranqueadosComponent implements OnInit {
  
  index: number = -1;
  aportes: Array<any> = [];
  
  ordens: SelectItem[];
  ordem;
  busca;
  
  total_rendimentos = 0;
  total_aportes = 0;
  total_franqueados = 0;
  
  franqueados;
  
  ngOnInit() {
    this.padroes.requestGET('/administrador/franqueados', (response) => {
      if (response.error) {
        return;
      }
      console.log(response);
      this.franqueados = response;
      this.franqueados.map((item) => {
        this.total_franqueados++;
        this.total_aportes = this.total_aportes + item.valor_aportes;
        this.total_rendimentos = this.total_rendimentos + item.valor_rendimentos;
        switch (item.genero) {
          case 'masculino':
            item.genero = (this.usuario.pais == 1 ? 'Male' : 'Masculino');
            break;
          case 'feminino':
            item.genero = (this.usuario.pais == 1 ? 'Female' : 'Feminino');
            break;
          case 'homosexual':
            item.genero = 'Homosexual';
            break;
          case 'transsexual':
            item.genero = 'Transsexual';
            break;
          case 'outro':
            item.genero = (this.usuario.pais == 1 ? 'Other' : 'Outro');
            break;
        }
        switch (item.estado_civil) {
          case 'casado':
            item.estado_civil = (this.usuario.pais == 1 ? 'Married' : 'Casado');
            break;
          case 'solteiro':
            item.estado_civil = (this.usuario.pais == 1 ? 'Unmarried' : 'Solteiro');
            break;
          case 'divorciado':
            item.estado_civil = (this.usuario.pais == 1 ? 'Divorced' : 'Divorciado');
            break;
          case 'viuvo':
            item.estado_civil = (this.usuario.pais == 1 ? 'Widower' : 'Viúvo');
            break;
          case 'outro':
            item.estado_civil = (this.usuario.pais == 1 ? 'Other' : 'Outro');
            break;
        }
        switch (item.moeda) {
          case 1:
            item.moeda = 'Libra';
            break;
          case 2:
            item.moeda = 'Real';
            break;
        }
        switch (item.pais) {
          case 1:
            item.pais = (this.usuario.pais == 1 ? 'England' : 'Inglaterra');
            break;
          case 2:
            item.pais = (this.usuario.pais == 1 ? 'Brazil' : 'Brasil');
            break;
        }
      });
    });
  }
  usuario: any = {};
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    this.usuario = this.padroes.verUsuario().usuario;
    this.ordens = [
      {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
      {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
    ];
  }
  
  input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
  fechar_busca_style = {'display': 'none'};
  
  abrir_busca() {
    this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
    this.fechar_busca_style = {'display': 'block'};
  }
  
  fechar_busca() {
    this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    this.fechar_busca_style = {'display': 'none'};
  }
  
  ordena() {
    this.padroes.requestGET('/administrador/franqueados?' + this.ordem.tipo + '=' + this.ordem.name, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.franqueados = response;
      
      console.log(this.franqueados);
    });
  }
  
  buscar() {
    this.padroes.requestGET('/administrador/franqueados?busca=' + this.busca, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.franqueados = response;
      
      console.log(this.franqueados);
    });
  }
  
  mensagemAtivarAdministrador() {
    this.messageService.add({severity:'success', summary: 'Franqueados ativado', detail: 'com sucesso'});
  }
  mensagemDesativarFranqueados() {
    this.messageService.add({severity:'success', summary: 'Franqueados desativado', detail: 'com sucesso'});
  }
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  
  style_certeza: any = {'opacity': '0', 'pointer-events': 'none'};
  editar_item: any = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
  certeza_abre(id_item, index_item, comprovante_id, funcao){
    this.style_certeza = {'opacity': '1', 'pointer-events': 'auto'};
    this.editar_item = {id_item: id_item, index_item: index_item, comprovante_id: comprovante_id, funcao: funcao};
  }
  cancela_certeza(){
    this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
    this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
  };
  
  desativar_usuario(usuario_id, index){
    this.padroes.requestGET('/administrador/desativar-franqueado/?franqueado_id='+usuario_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemDesativarFranqueados();
      this.franqueados[index].ativo = 0;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }
  
  ativar_usuario(usuario_id, index){
    this.padroes.requestGET('/administrador/ativar-franqueado/?franqueado_id='+usuario_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemAtivarAdministrador();
      this.franqueados[index].ativo = 1;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }

}
