import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-novos-aportes',
  templateUrl: './admin-novos-aportes.component.html',
  styleUrls: ['./admin-novos-aportes.component.css']
})
export class AdminNovosAportesComponent implements OnInit {

  current: any = '';
  index: number = -1;
  aportes: Array<any> = [];
  dataHoje: Date;

  images: any[];
  ordens: SelectItem[];
  ordem;
  busca;
  urlComprovante;

  total_novos;
  total_analise;
  total_sem_comprovante;

  usuario;
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    this.usuario = this.padroes.verUsuario().usuario;
    this.ordens = [
        {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
        {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
        {label: (this.usuario.pais == 1 ? 'Hight value' : 'Maior Valor'), value: {id: 3, name: 'maior-valor', tipo: 'ordem'}},
        {label: (this.usuario.pais == 1 ? 'Down value' : 'Menor Valor'), value: {id: 4, name: 'menor-valor', tipo: 'ordem'}},
    ];
  }
  
  mensagemComprovanteInserido() {
    this.messageService.add({severity:'success', summary: 'Comprovante inserido', detail: 'com sucesso'});
  }
  mensagemComprovanteRemovido() {
    this.messageService.add({severity:'success', summary: 'Comprovante removido', detail: 'com sucesso'});
  }
  mensagemAporteRemovido() {
    this.messageService.add({severity:'success', summary: 'Aporte removido', detail: 'com sucesso'});
  }
  mensagemAporteAprovado() {
    this.messageService.add({severity:'success', summary: 'Aporte ativo', detail: 'com sucesso'});
  }
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  
  upload(file, id, index, form) {

    let formData: FormData = new FormData();
    let files = file.files[0];

    formData.append('arquivo', files, files.name);

    this.padroes.requestPOST('/administrador/inserir-comprovante?aporte_id='+id, formData, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemComprovanteInserido();
      if(this.aportes[index].situacao == 1){
        this.aportes[index].situacao = 2;
        this.total_analise++;
        this.total_sem_comprovante--;
      }

      this.aportes[index].comprovantes = response;
      console.log(response);
      form.clear();
    });
  }
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.urlComprovante = this.padroes.urlComprovante;
    this.padroes.requestGET('/administrador/aportes?filtro=nao-aprovados', (response) => {
      if(response.error){
        return;
      }
      this.aportes = response;
    });

    this.padroes.requestGET('/me', (response) => {
      if(response.error){
        return;
      }
      this.total_novos = response.total_aportes_sem_comprovante + response.total_aportes_em_analise;
      this.total_analise = response.total_aportes_em_analise;
      this.total_sem_comprovante = response.total_aportes_sem_comprovante;
    });
  }
  
  input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
  input_busca_style_abrir = {'display': 'inline-block'};
  fechar_busca_style = {'display': 'none'};
  abrir_busca(){
    this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
    this.input_busca_style_abrir = {'display': 'none'};
    this.fechar_busca_style = {'display': 'block'};
  }
  fechar_busca(){
    this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    this.fechar_busca_style = {'display': 'none'};
    this.input_busca_style_abrir = {'display': 'inline-block'};
  }

  ordena(){
    this.padroes.requestGET('/administrador/aportes?filtro=nao-aprovados&'+this.ordem.tipo+'='+this.ordem.name, (response) => {
      if(response.error){
        return;
      }
      this.aportes = response;

      console.log(this.aportes);
    });
  }

  buscar(){
    this.padroes.requestGET('/administrador/aportes?filtro=nao-aprovados&busca='+this.busca, (response) => {
      if(response.error){
        return;
      }
      this.aportes = response;

      console.log(this.aportes);
    });
  }
  
  aprova_aporte(id, index){
    this.padroes.requestGET('/administrador/ativar-aporte/'+id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemAporteAprovado();
      let estava = this.aportes[index].situacao;
      if(estava == 1){
        this.total_sem_comprovante--;
      }else{
        this.total_analise--;
      }
      this.total_novos--;
      this.aportes[index].situacao = 3;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }

  apagar_comprovante(id, aporte_id, index){
    this.padroes.requestGET('/administrador/apagar-comprovante/'+id+'/?aporte_id='+aporte_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemComprovanteRemovido();
      if(response.length == 0 && this.aportes[index].situacao == 2){
        this.aportes[index].situacao = 1;
        this.total_analise--;
        this.total_sem_comprovante++;
      }
      this.aportes[index].comprovantes = response;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }

  apagar_aporte(id){
    this.padroes.requestGET('/administrador/apagar-aporte/'+id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemAporteRemovido();
      let novo_aporte = [];
      this.aportes.filter((aporte) =>{
        if(aporte.id != id){
          novo_aporte.push(aporte);
        }
      });
      this.aportes = novo_aporte;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }
  
  style_certeza: any = {'opacity': '0', 'pointer-events': 'none'};
  editar_item: any = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
  certeza_abre(id_item, index_item, comprovante_id, funcao){
    this.style_certeza = {'opacity': '1', 'pointer-events': 'auto'};
    this.editar_item = {id_item: id_item, index_item: index_item, comprovante_id: comprovante_id, funcao: funcao}
  }
  cancela_certeza(){
    this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
    this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
  };

}
