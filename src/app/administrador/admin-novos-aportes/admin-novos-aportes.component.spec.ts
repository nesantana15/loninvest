import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNovosAportesComponent } from './admin-novos-aportes.component';

describe('AdminNovosAportesComponent', () => {
  let component: AdminNovosAportesComponent;
  let fixture: ComponentFixture<AdminNovosAportesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNovosAportesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNovosAportesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
