import {Component, OnInit} from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-admin-saques-clientes',
    templateUrl: './admin-saques-clientes.component.html',
    styleUrls: ['./admin-saques-clientes.component.css']
})
export class AdminSaquesClientesComponent implements OnInit {

    current: any = '';
    index: number = -1;
    saques: Array<any> = [];
    dataHoje: Date;

    images: any[];
    ordens: SelectItem[];
    ordem;
    busca;
    urlComprovante;
    
    solicitacoes_liberadas = 0;
    total_solicitacoes = 0;
    usuario;

    constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
        this.usuario = this.padroes.verUsuario().usuario;
        this.ordens = [
            {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
            {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
            {label: (this.usuario.pais == 1 ? 'Hight value' : 'Maior Valor'), value: {id: 3, name: 'maior-valor', tipo: 'ordem'}},
            {label: (this.usuario.pais == 1 ? 'Down value' : 'Menor Valor'), value: {id: 4, name: 'menor-valor', tipo: 'ordem'}},
        ];
    }

    ngOnInit() {
        this.usuario = this.padroes.verUsuario().usuario;
        this.urlComprovante = this.padroes.urlComprovante;
        this.padroes.requestGET('/administrador/saque-cliente', (response) => {
            if (response.error) {
                return;
            }
            this.saques = response;
            this.total_solicitacoes = this.saques.length;
            this.saques.map((saque) => {
                if(saque.situacao == 2){
                    this.solicitacoes_liberadas++;
                }
            });
        });
    }

    input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    fechar_busca_style = {'display': 'none'};

    abrir_busca() {
        this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
        this.fechar_busca_style = {'display': 'block'};
    }

    fechar_busca() {
        this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
        this.fechar_busca_style = {'display': 'none'};
    }

    ordena() {
        this.padroes.requestGET('/administrador/saque-cliente?' + this.ordem.tipo + '=' + this.ordem.name, (response) => {
            if (response.error) {
                return;
            }
            this.saques = response;

            console.log(this.saques);
        });
    }

    buscar() {
        this.padroes.requestGET('/administrador/saque-cliente?busca=' + this.busca, (response) => {
            if (response.error) {
                return;
            }
            this.saques = response;

            console.log(this.saques);
        });
    }
    
    
    mensagemDepositoRealizado() {
        this.messageService.add({severity:'success', summary: 'Deposito realizado', detail: 'com sucesso'});
    }
    mensagemErroInesperado() {
        this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
    }
    
    confirmar_deposito(id, index) {
        this.padroes.requestGET('/administrador/confirmar-deposito-cliente/'+id, (response) => {
            if (response.error) {
                this.mensagemErroInesperado();
                return;
            }
            this.mensagemDepositoRealizado();
            this.saques[index].data_deposito = response.data_deposito;
            this.saques[index].situacao = 2;
        });
    }

}
