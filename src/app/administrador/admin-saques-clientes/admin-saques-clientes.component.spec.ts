import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSaquesClientesComponent } from './admin-saques-clientes.component';

describe('AdminSaquesClientesComponent', () => {
  let component: AdminSaquesClientesComponent;
  let fixture: ComponentFixture<AdminSaquesClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSaquesClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSaquesClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
