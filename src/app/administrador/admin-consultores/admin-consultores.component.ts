import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-consultores',
  templateUrl: './admin-consultores.component.html',
  styleUrls: ['./admin-consultores.component.css']
})
export class AdminConsultoresComponent implements OnInit {

  index: number = -1;
  aportes: Array<any> = [];

  ordens: SelectItem[];
  ordem;
  busca;

  total_rendimentos = 0;
  total_aportes = 0;
  total_consultores = 0;

  consultores;
  urlContrato;

  ngOnInit() {
    this.urlContrato = this.padroes.urlContrato;
    this.padroes.requestGET('/administrador/consultores', (response) => {
      if (response.error) {
        return;
      }
      this.consultores = response;
      this.consultores.map((item) => {
        this.total_consultores++;
        this.total_aportes = this.total_aportes + item.valor_aportes;
        this.total_rendimentos = this.total_rendimentos + item.valor_rendimentos;
        switch (item.genero) {
          case 'masculino':
            item.genero = (this.usuario.pais == 1 ? 'Male' : 'Masculino');
            break;
          case 'feminino':
            item.genero = (this.usuario.pais == 1 ? 'Female' : 'Feminino');
            break;
          case 'homosexual':
            item.genero = 'Homosexual';
            break;
          case 'transsexual':
            item.genero = 'Transsexual';
            break;
          case 'outro':
            item.genero = (this.usuario.pais == 1 ? 'Other' : 'Outro');
            break;
        }
        switch (item.estado_civil) {
          case 'casado':
            item.estado_civil = (this.usuario.pais == 1 ? 'Married' : 'Casado');
            break;
          case 'solteiro':
            item.estado_civil = (this.usuario.pais == 1 ? 'Unmarried' : 'Solteiro');
            break;
          case 'divorciado':
            item.estado_civil = (this.usuario.pais == 1 ? 'Divorced' : 'Divorciado');
            break;
          case 'viuvo':
            item.estado_civil = (this.usuario.pais == 1 ? 'Widower' : 'Viúvo');
            break;
          case 'outro':
            item.estado_civil = (this.usuario.pais == 1 ? 'Other' : 'Outro');
            break;
        }
        switch (item.moeda) {
          case 1:
            item.moeda = 'Libra';
            break;
          case 2:
            item.moeda = 'Real';
            break;
        }
        switch (item.pais) {
          case 1:
            item.pais = (this.usuario.pais == 1 ? 'England' : 'Inglaterra');
            break;
          case 2:
            item.pais = (this.usuario.pais == 1 ? 'Brazil' : 'Brasil');
            break;
        }
      });
      console.log(response);
    });
  }

  upload(file, id, index, form) {

    let formData: FormData = new FormData();
    let files = file.files[0];

    formData.append('arquivo', files, files.name);

    this.padroes.requestPOST('/administrador/inserir-contrato-consultor/?consultor_id='+id, formData, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemContratoInserido();
      this.consultores[index].contratos = response;
      console.log(response);
      form.clear();
    });
  }
  
  usuario: any = {};

  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    this.usuario = this.padroes.verUsuario().usuario;
    this.ordens = [
      {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
      {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
    ];
  }
  
  input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
  input_busca_style_abrir = {'display': 'inline-block'};
  fechar_busca_style = {'display': 'none'};
  abrir_busca(){
    this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
    this.input_busca_style_abrir = {'display': 'none'};
    this.fechar_busca_style = {'display': 'block'};
  }
  fechar_busca(){
    this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    this.fechar_busca_style = {'display': 'none'};
    this.input_busca_style_abrir = {'display': 'inline-block'};
  }
  
  
  mensagemContratoInserido() {
    this.messageService.add({severity:'success', summary: 'Contrato inserido', detail: 'com sucesso'});
  }
  mensagemContratoRemovido() {
    this.messageService.add({severity:'success', summary: 'Contrato removido', detail: 'com sucesso'});
  }
  mensagemAtivarConsultor() {
    this.messageService.add({severity:'success', summary: 'Consultor ativado', detail: 'com sucesso'});
  }
  mensagemDesativarConsultor() {
    this.messageService.add({severity:'success', summary: 'Consultor desativado', detail: 'com sucesso'});
  }
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  
  ordena() {
    this.padroes.requestGET('/administrador/consultores?' + this.ordem.tipo + '=' + this.ordem.name, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.consultores = response;

      console.log(this.consultores);
    });
  }

  buscar() {
    this.padroes.requestGET('/administrador/consultores?busca=' + this.busca, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.consultores = response;

      console.log(this.consultores);
    });
  }
  
  style_certeza: any = {'opacity': '0', 'pointer-events': 'none'};
  editar_item: any = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
  certeza_abre(id_item, index_item, comprovante_id, funcao){
    this.style_certeza = {'opacity': '1', 'pointer-events': 'auto'};
    this.editar_item = {id_item: id_item, index_item: index_item, comprovante_id: comprovante_id, funcao: funcao}
  }
  cancela_certeza(){
    this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
    this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
  };

  apagar_contrato(id, consultor_id, index){
    this.padroes.requestGET('/administrador/apagar-contrato-consultor/'+id+'/?consultor_id='+consultor_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemContratoRemovido();
      this.consultores[index].contratos = response;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }



  desativar_usuario(usuario_id, index){
    this.padroes.requestGET('/administrador/desativar-consultor/?consultor_id='+usuario_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemDesativarConsultor();
      this.consultores[index].ativo = 0;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }

  ativar_usuario(usuario_id, index){
    this.padroes.requestGET('/administrador/ativar-consultor/?consultor_id='+usuario_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemAtivarConsultor();
      this.consultores[index].ativo = 1;
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }


  reseta_banco(consultor_id, index){
    this.padroes.requestGET('/administrador/resetar-banco-consultor?consultor_id='+consultor_id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.messageService.add({severity:'success', summary: 'Banco resetado', detail: 'com sucesso'});
      this.consultores[index].banco = '';
      this.consultores[index].agencia = '';
      this.consultores[index].conta_corrente = '';
      this.style_certeza = {'opacity': '0', 'pointer-events': 'none'};
      this.editar_item = {id_item: null, index_item: null, comprovante_id: null, funcao: null};
    });
  }

}
