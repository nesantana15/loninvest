import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConsultoresComponent } from './admin-consultores.component';

describe('AdminConsultoresComponent', () => {
  let component: AdminConsultoresComponent;
  let fixture: ComponentFixture<AdminConsultoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConsultoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConsultoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
