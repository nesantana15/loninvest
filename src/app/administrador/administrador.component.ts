import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
      setInterval(() => {
          console.log('teste');
          this.padroes.requestGET('/administrador/totais', (response) => {
              console.log(response);
	          this.novos_aportes = response.total_aportes_nao_aprovados;
	          this.saques_cliente = response.total_solicitacoes_saque_cliente;
	          this.saques_consultor = response.total_solicitacoes_saque_consultor;
            this.contato = response.total_contatos_nao_respondidos;
          });
      }, 1000*30);
  }
  
  usuario: any = {};
  tipo_cliente;

  novos_aportes;
  saques_consultor;
  saques_franqueados;
  saques_cliente;
  contato;


    mensagemSaiu() {
        this.messageService.add({severity:'success', summary: 'Usuário saiu', detail: 'com sucesso'});
    }

    mensagemErroInesperado() {
        this.messageService.add({severity:'error', summary: 'Opaa, aconteceu um erro', detail: 'ao tentar sair, mas já estamos arrumando.'});
    }

    sair(){
        this.padroes.requestGET('/logout', (response) => {
            if (response.error) {
                this.mensagemErroInesperado();
                return;
            }
            this.mensagemSaiu();
            this.padroes.setarUsuario(null);
            this.router.navigate(['/']);
        });
    }
  
  ngOnInit() {
    this.padroes.requestGET('/me', (response) => {
      this.usuario = response;
      if(this.usuario.tipo == 1){
      	console.log(response);
      	if(this.usuario.pais == 1){
	        this.tipo_cliente = 'Administrator';
        }else{
	        this.tipo_cliente = 'Administrador';
        }
        this.padroes.requestGET('/administrador/totais', (response) => {
        	console.log(response);
	        this.novos_aportes = response.total_aportes_nao_aprovados;
	        this.saques_cliente = response.total_solicitacoes_saque_cliente;
            this.saques_consultor = response.total_solicitacoes_saque_consultor;
            this.saques_franqueados = response.total_solicitacoes_saque_franqueado;
          this.contato = response.total_contatos_nao_respondidos;
        });
      }else if(this.usuario.tipo == 2){
        this.router.navigate(['/consultant']);
      }else if(this.usuario.tipo == 3){
        this.router.navigate(['/client']);
      }else{
          this.router.navigate(['/franchisee']);
      }
    });
  }

}
