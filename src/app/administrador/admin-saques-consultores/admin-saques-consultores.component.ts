import {Component, OnInit} from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-admin-saques-consultores',
    templateUrl: './admin-saques-consultores.component.html',
    styleUrls: ['./admin-saques-consultores.component.css']
})
export class AdminSaquesConsultoresComponent implements OnInit {

    index: number = -1;
    saques: Array<any> = [];

    ordem;
    busca;
    urlComprovante;
    
    solicitacoes_liberadas = 0;
    total_solicitacoes = 0;
    
    usuario;

    constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    }

    ngOnInit() {
        this.usuario = this.padroes.verUsuario().usuario;
        this.urlComprovante = this.padroes.urlComprovante;
        this.padroes.requestGET('/administrador/saque-consultor', (response) => {
            if (response.error) {
                return;
            }
            this.saques = response;
            this.total_solicitacoes = this.saques.length;
            this.saques.map((saque) => {
                if(saque.situacao == 2){
                    this.solicitacoes_liberadas++;
                }
            });
        });
        
    }
    
    mensagemDepositoRealizado() {
        this.messageService.add({severity:'success', summary: 'Deposito realizado', detail: 'com sucesso'});
    }
    mensagemErroInesperado() {
        this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
    }
    
    confirmar_deposito(id, index) {
        this.padroes.requestGET('/administrador/confirmar-deposito-consultor/' + id, (response) => {
            if (response.error) {
                this.mensagemErroInesperado();
                return;
            }
            this.mensagemDepositoRealizado();
            this.saques[index].data_deposito = response.data_deposito;
            this.saques[index].situacao = 2;
        });
    }

}
