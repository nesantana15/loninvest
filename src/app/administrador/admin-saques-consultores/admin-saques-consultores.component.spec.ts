import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSaquesConsultoresComponent } from './admin-saques-consultores.component';

describe('AdminSaquesConsultoresComponent', () => {
  let component: AdminSaquesConsultoresComponent;
  let fixture: ComponentFixture<AdminSaquesConsultoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSaquesConsultoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSaquesConsultoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
