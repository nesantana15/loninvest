import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSaquesFranqueadosComponent } from './admin-saques-franqueados.component';

describe('AdminSaquesFranqueadosComponent', () => {
  let component: AdminSaquesFranqueadosComponent;
  let fixture: ComponentFixture<AdminSaquesFranqueadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSaquesFranqueadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSaquesFranqueadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
