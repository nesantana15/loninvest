import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAportesComponent } from './admin-aportes.component';

describe('AdminAportesComponent', () => {
  let component: AdminAportesComponent;
  let fixture: ComponentFixture<AdminAportesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAportesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAportesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
