import {Component, OnInit} from '@angular/core';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService, SelectItem} from 'primeng/api';
import {DatePipe} from '@angular/common';

@Component({
    selector: 'app-admin-movimentacoes',
    templateUrl: './admin-movimentacoes.component.html',
    styleUrls: ['./admin-movimentacoes.component.css']
})
export class AdminMovimentacoesComponent implements OnInit {

    index: number = -1;
    
    primeiro_dia: Date = new Date(2019, 0, 1);
    segundo_dia: Date = new Date(Date.now());
    
    en: any;
    pt: any;
    
    mostra_data(){
        let primeiro = this.datepipe.transform(this.primeiro_dia, 'yyyy-MM-dd');
        let segundo = this.datepipe.transform(this.segundo_dia, 'yyyy-MM-dd');
        
        let ordem_aqui;
        
        if(this.ordem){
            ordem_aqui = this.ordem.tipo+'='+this.ordem.name+'&';
        }
        
        this.padroes.requestGET('/administrador/movimentacoes-cliente?'+(ordem_aqui != undefined ? ordem_aqui : '')+'data_inicio='+primeiro+'&data_termino='+segundo, (response) => {
            if (response.error) {
                return;
            }
            this.movimentacoes = response;
        });
    };

    numero_movimentacoes = 0;
    valor_depositos = 0;
    valor_saques = 0; 
    movimentacoes;

    busca;
    ordens: SelectItem[];
    ordem;

    usuario: any = {};

    ngOnInit() {
        this.usuario = this.padroes.verUsuario().usuario;
        this.pt = {
          firstDayOfWeek: 0,
          dayNames: ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"],
          dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
          dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
          monthNames: [ "Janeiro","Fevereiro","Março","Abril","Maio","Junho","Júlio","Agosto","Setembro","Outubro","Novembro","Dezembro" ],
          monthNamesShort: [ "Jan", "Fev", "Mar", "Abr", "Mai", "Jun","Jul", "Ago", "Set", "Out", "Nov", "Dez" ],
          today: 'Hoje',
          clear: 'Limpar',
          dateFormat: 'dd/mm/yy'
        };
        this.en = {
            firstDayOfWeek: 0,
            dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            dayNamesMin: ["Su","Mo","Tu","We","Th","Fr","Sa"],
            monthNames: [ "January","February","March","April","May","June","July","August","September","October","November","December" ],
            monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
            today: 'Today',
            clear: 'Clear',
            dateFormat: 'mm/dd/yy'
        };
        this.padroes.requestGET('/administrador/movimentacoes-cliente', (response) => {
            if (response.error) {
                return;
            }
            this.movimentacoes = response;
            this.movimentacoes.map((item) => {
                this.numero_movimentacoes++;
                if(item.tipo == 1){
                    this.valor_depositos = this.valor_depositos + item.valor;
                }else{
                    this.valor_saques = this.valor_saques + item.valor;
                }
            });
            console.log(response);
        });
    }
    
    tipo_usuario = 3;
    
    trocar_movimentacao(tipo_usuario){
        this.tipo_usuario = tipo_usuario;
        if(tipo_usuario == 2){
            this.padroes.requestGET('/administrador/movimentacoes-consultor', (response) => {
                if (response.error) {
                    return;
                }
                this.movimentacoes = response;
                this.movimentacoes.map((item) => {
                    this.numero_movimentacoes++;
                    if(item.tipo == 1){
                        this.valor_depositos = this.valor_depositos + item.valor;
                    }else{
                        this.valor_saques = this.valor_saques + item.valor;
                    }
                });
                console.log(response);
            });
        }else if(tipo_usuario == 3){
            this.padroes.requestGET('/administrador/movimentacoes-cliente', (response) => {
                if (response.error) {
                    return;
                }
                this.movimentacoes = response;
                this.movimentacoes.map((item) => {
                    this.numero_movimentacoes++;
                    if(item.tipo == 1){
                        this.valor_depositos = this.valor_depositos + item.valor;
                    }else{
                        this.valor_saques = this.valor_saques + item.valor;
                    }
                });
                console.log(response);
            });
        }else{
            this.padroes.requestGET('/administrador/movimentacoes-franqueado', (response) => {
                if (response.error) {
                    return;
                }
                this.movimentacoes = response;
                this.movimentacoes.map((item) => {
                    this.numero_movimentacoes++;
                    if(item.tipo == 1){
                        this.valor_depositos = this.valor_depositos + item.valor;
                    }else{
                        this.valor_saques = this.valor_saques + item.valor;
                    }
                });
                console.log(response);
            });
        }
    }
    
    input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    fechar_busca_style = {'display': 'none'};

    abrir_busca() {
        this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
        this.fechar_busca_style = {'display': 'block'};
    }

    fechar_busca() {
        this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
        this.fechar_busca_style = {'display': 'none'};
    }

    ordena() {
        let primeiro = this.datepipe.transform(this.primeiro_dia, 'yyyy-MM-dd');
        let segundo = this.datepipe.transform(this.segundo_dia, 'yyyy-MM-dd');
        this.padroes.requestGET('/administrador/'+(this.tipo_usuario == 2 ? 'movimentacoes-consultor' : (this.tipo_usuario == 4 ? 'movimentacoes-franqueado' : 'movimentacoes-cliente'))+'?' + this.ordem.tipo + '=' + this.ordem.name+'&data_inicio='+primeiro+'&data_termino='+segundo, (response) => {
            if (response.error) {
                return;
            }
            this.movimentacoes = response;
        });
    }

    buscar() {
        this.padroes.requestGET('/administrador'+(this.tipo_usuario == 2 ? 'movimentacoes-consultor' : (this.tipo_usuario == 4 ? 'movimentacoes-franqueado' : 'movimentacoes-cliente'))+'?busca=' + this.busca, (response) => {
            if (response.error) {
                return;
            }
            this.movimentacoes = response;
        });
    }
    
    constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService, public datepipe: DatePipe) {
        this.usuario = this.padroes.verUsuario().usuario;
        this.ordens = [
            {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
            {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
            {label: (this.usuario.pais == 1 ? 'News Applications' : 'Depositos'), value: {id: 3, name: 'depositos', tipo: 'filtro'}},
            {label: (this.usuario.pais == 1 ? 'Withdrawal' : 'Saques'), value: {id: 4, name: 'saques', tipo: 'filtro'}},
        ];
    }

}
