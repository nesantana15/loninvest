import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMovimentacoesComponent } from './admin-movimentacoes.component';

describe('AdminMovimentacoesComponent', () => {
  let component: AdminMovimentacoesComponent;
  let fixture: ComponentFixture<AdminMovimentacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMovimentacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMovimentacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
