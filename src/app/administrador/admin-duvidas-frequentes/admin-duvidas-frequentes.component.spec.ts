import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDuvidasFrequentesComponent } from './admin-duvidas-frequentes.component';

describe('AdminDuvidasFrequentesComponent', () => {
  let component: AdminDuvidasFrequentesComponent;
  let fixture: ComponentFixture<AdminDuvidasFrequentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDuvidasFrequentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDuvidasFrequentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
