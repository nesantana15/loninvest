import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

@Injectable({
  providedIn: 'root'
})
export class PadroesService {

    constructor(private http: HttpClient, private router: Router) {
        if(this.verUsuario()){
            this.urlComprovante = this.urlPadrao('/administrador/inserir-comprovante');
            this.urlContrato = this.urlPadrao('/administrador/inserir-contrato');
        }
    }
  
    
    static baseUrl: string = 'https://londoneyesinvest.com:30001';
    static apiUrl: string = PadroesService.baseUrl+'/api';

    urlComprovante;
    urlContrato;

    caminhoImagemComprovante(cliente, aporte, id_imagem, extensao_imagem){
        return PadroesService.baseUrl+'/storage/comprovantes/'+cliente+'/'+aporte+'/'+id_imagem+'.'+extensao_imagem;
    }
    caminhoImagemContratoCliente(cliente, id_imagem, extensao_imagem){
        return PadroesService.baseUrl+'/storage/contratos/clientes/'+cliente+'/'+id_imagem+'.'+extensao_imagem;
    }
    caminhoImagemContratoConsultores(consultor, id_imagem, extensao_imagem){
        return PadroesService.baseUrl+'/storage/contratos/consultores/'+consultor+'/'+id_imagem+'.'+extensao_imagem;
    }

    urlPadrao(url){
        let caracter;
        if(url.indexOf('?') == -1){
            caracter = '?';
        }else{
            caracter = '&';
        }
        return PadroesService.apiUrl+url+(caracter)+'accessToken='+this.verUsuario().accessToken;
    }

    requestPOST(url, form, callback){
        this.http.post(this.urlPadrao(url), form)
            .pipe(map(data => data || []))
            .subscribe((response: any) => {
                if(response.error){
                    if(response.auth){
                        this.setarUsuario({'accessToken': '', 'usuario': null});
                        location.href = "/";
                        console.log(response);
                    }
                    console.log(response);
                }
                callback(response);
            });
    }


    usuarioLogin(url, form, callback){
        this.http.post(PadroesService.apiUrl+url, form)
            .pipe(map(data => data || []))
            .subscribe((response: any) => {
                if(response.error && response.auth){
                    return console.log(response);
                }
                callback(response);
            });
    }

    requestGET(url, callback){
        this.http.get(this.urlPadrao(url))
            .pipe(map(data => data || []))
            .subscribe((response: any) => {
                if(response.error){
                    if(response.auth){
                        this.setarUsuario({'accessToken': '', 'usuario': null});
                        location.href = "/";
                        return console.log(response);
                    }
                    return console.log(response);
                }
                callback(response);
            });
    }

    setarUsuario(usuario){
        localStorage.setItem("usuario", JSON.stringify(usuario));
    }

    verUsuario(){
        return JSON.parse(localStorage.getItem("usuario"));
    }



    // requestPOST(url) {
    //     return this.http.post(PadroesService.apiUrl, url, httpOptions)
    //         .pipe();
    // }

}
