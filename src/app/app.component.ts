import { Component } from '@angular/core';
import {PadroesService} from './funcoes/padroes.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PadroesService, MessageService]
})
export class AppComponent {
  
  
  constructor(private padroes: PadroesService){}
  
  
}
