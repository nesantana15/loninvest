import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-criar-contato',
  templateUrl: './criar-contato.component.html',
  styleUrls: ['./criar-contato.component.css']
})
export class CriarContatoComponent implements OnInit {
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService, private route: ActivatedRoute) { }
  
  ngOnInit() {}
  
  
  mensagemCriou() {
    this.messageService.add({severity:'success', summary: 'Contato', detail: 'criado com sucesso'});
  }
  
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  
  enviar(form){
    console.log(form);
    this.padroes.requestPOST('/cliente/inserir-contato/', form, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemCriou();
      this.router.navigate(['/cliente/contact']);
    });
  }
}
