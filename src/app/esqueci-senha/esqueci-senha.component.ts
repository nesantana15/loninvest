import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-esqueci-senha',
  templateUrl: './esqueci-senha.component.html',
  styleUrls: ['./esqueci-senha.component.css']
})
export class EsqueciSenhaComponent implements OnInit {
  
  enviar(form, event) {
    event.preventDefault();
    this.padroes.usuarioLogin('/esqueci-senha', form, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemEditou();
      this.padroes.setarUsuario(response);
      switch (response.usuario.tipo) {
        case 1:
          this.router.navigate(['/manager']);
          break;
        case 2:
          this.router.navigate(['/consultant']);
          break;
        case 3:
          this.router.navigate(['/client']);
          break;
        case 4:
          this.router.navigate(['/franchisee']);
          break;
      }
    });
  }
  
  mensagemEditou() {
    this.messageService.add({severity:'success', summary: 'Senha alterada', detail: 'com sucesso'});
  }
  
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
  }
  
  usuario_lodago: any = {};
  ngOnInit() {
  }

}
