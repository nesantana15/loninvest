import {Component, OnInit} from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    enviar(form, event) {
        event.preventDefault();
        this.padroes.usuarioLogin('/login', form, (response) => {
            if(response.error){
                this.mensagemErroInesperado();
                return;
            }
            this.mensagemLogou();
            this.padroes.setarUsuario(response);
            if(response.usuario.trocar_senha == 1){
                this.router.navigate(['/change-password']);
                return;
            }
            switch (response.usuario.tipo) {
                case 1:
                    this.router.navigate(['/manager']);
                    break;
                case 2:
                    this.router.navigate(['/consultant']);
                    break;
                case 3:
                    this.router.navigate(['/client']);
                    break;
                case 4:
                    this.router.navigate(['/franchisee']);
                    break;
            }
        });
    }
    
    mensagemLogou() {
        this.messageService.add({severity:'success', summary: 'Login efetuado', detail: 'com sucesso'});
    }
    
    mensagemErroInesperado() {
        this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
    }
    constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    }

    usuario_lodago: any = {};
    ngOnInit() {
        this.usuario_lodago = this.padroes.verUsuario().usuario;
        if(this.usuario_lodago != null){
            this.padroes.requestGET('/me', (response) => {
                let accessToken = response.accessToken;
                if(accessToken != ""){
                    if(this.usuario_lodago.tipo == 1){
                        this.router.navigate(['/manager']);
                    }else if(this.usuario_lodago.tipo == 2){
                        this.router.navigate(['/consultant']);
                    }else if(this.usuario_lodago.tipo == 3){
                        this.router.navigate(['/client']);
                    }else{
                        this.router.navigate(['/franchisee']);
                    }
                }
            });
        }
    }

}
