import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-nova-duvidas-frequentes',
  templateUrl: './nova-duvidas-frequentes.component.html',
  styleUrls: ['./nova-duvidas-frequentes.component.css']
})
export class NovaDuvidasFrequentesComponent implements OnInit {
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService, private route: ActivatedRoute) { }
  
  consultores;
  tipo_do_usuario;
  novo_usuario = '';
  usuario;
  
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.route.params.subscribe(params => {
      this.novo_usuario = params['novo_usuario'];
    });
    
    let usuario = this.padroes.verUsuario().usuario;
    this.tipo_do_usuario = usuario.tipo;
    
    if(this.tipo_do_usuario == 1) {
      this.padroes.requestGET('/administrador/novo-cliente', (response) => {
        if (response.error) {
          return;
        }
        this.consultores = response;
      });
    }else if(this.tipo_do_usuario == 2){
      this.novo_usuario = '3';
    }else{
      this.router.navigate(['/client/applications']);
    }
    
  }
  
  
  mensagemCriou() {
    this.messageService.add({severity:'success', summary: 'Dúvida criada', detail: 'com sucesso'});
  }
  
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  
  enviar(form){
      if(this.tipo_do_usuario == 1){
        this.padroes.requestPOST('/administrador/inserir-duvida-frequente', form, (response) => {
          if (response.error) {
            this.mensagemErroInesperado();
            return;
          }
          this.mensagemCriou();
          this.router.navigate(['/manager/questions']);
        });
      }
    console.log(form);
  }

}
