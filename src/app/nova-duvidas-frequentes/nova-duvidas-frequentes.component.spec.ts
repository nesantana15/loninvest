import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovaDuvidasFrequentesComponent } from './nova-duvidas-frequentes.component';

describe('NovaDuvidasFrequentesComponent', () => {
  let component: NovaDuvidasFrequentesComponent;
  let fixture: ComponentFixture<NovaDuvidasFrequentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovaDuvidasFrequentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovaDuvidasFrequentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
