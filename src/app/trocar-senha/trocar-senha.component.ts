import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-trocar-senha',
  templateUrl: './trocar-senha.component.html',
  styleUrls: ['./trocar-senha.component.css']
})
export class TrocarSenhaComponent implements OnInit {

  usuario;

  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) { }

  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
  }
  
  voltar(){
    this.router.navigate(['/']);
  }
  
  enviar(form){
    console.log(form);
    if(form.nova_senha == form.confirmar_nova_senha){
      this.padroes.requestPOST('/trocar-senha', form, (response) => {
        if (response.error) {
          return console.log('erro');
        }
        let novo_usuario = this.padroes.verUsuario();
        novo_usuario.usuario.trocar_senha = 0;
        this.padroes.setarUsuario(novo_usuario);
        console.log(this.padroes.verUsuario());
        if(this.usuario.tipo == 1){
          this.router.navigate(['/manager/']);
        }else if(this.usuario.tipo == 2){
          this.router.navigate(['/consultant/']);
        }else{
          this.router.navigate(['/client/']);
        }
      });
    }else{
      // erro
    }
  }

  addSingle() {
    this.messageService.add({severity:'success', summary: 'Troca de senha', detail: 'Efetuada com sucesso'});
  }

}
