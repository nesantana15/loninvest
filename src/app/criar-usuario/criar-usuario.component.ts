import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-criar-usuario',
  templateUrl: './criar-usuario.component.html',
  styleUrls: ['./criar-usuario.component.css']
})
export class CriarUsuarioComponent implements OnInit {

  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService, private route: ActivatedRoute) { }

  consultores: Array<any> = [];
  franqueados: Array<any> = [];
  tipo_do_usuario;
  novo_usuario = '';
  cliente_consultor = '';
  franqueado_cliente_consultor = '';
  usuario;
  pais_selecionado = '2';
  
  ngOnInit() {

    this.route.params.subscribe(params => {
      this.novo_usuario = params['novo_usuario'];
    });

    this.usuario = this.padroes.verUsuario().usuario;
    this.tipo_do_usuario = this.usuario.tipo;


    if(this.tipo_do_usuario == 1) {
      this.padroes.requestGET('/administrador/novo-cliente', (response) => {
        if (response.error) {
          return;
        }
        console.log(response);
        this.consultores = response.consultores;
        this.franqueados = response.franqueados;
      });
    }else if(this.tipo_do_usuario == 2){
      this.novo_usuario = '3';
    }else if(this.tipo_do_usuario == 4) {
      this.padroes.requestGET('/franqueado/novo-cliente', (response) => {
        if (response.error) {
          return;
        }
        console.log(response);
        this.consultores = response;
      });
    }else{
      this.router.navigate(['/client/applications']);
    }

  }
  
  troca_franqueado(){
    this.consultores.map((consultor) => {
      if(consultor.id == this.cliente_consultor){
        console.log(consultor);
        this.franqueado_cliente_consultor = consultor.consultor_franqueado;
      }
    });
  }

  mensagemCriou(text) {
    this.messageService.add({severity:'success', summary: text, detail: 'criado com sucesso, o cliente receberá um e-mail com a senha.'});
  }

  mensagemJaCadastrado() {
    this.messageService.add({severity:'error', summary: 'Usuário', detail: 'ja cadastrado'});
  }

  mensagemSemPermissao() {
    this.messageService.add({severity:'error', summary: 'Sem permissão', detail: 'Você não tem permissão para isso.'});
  }

  enviar(form){
    if(this.novo_usuario == '1'){
      if(this.tipo_do_usuario == 1){
        this.padroes.requestPOST('/administrador/inserir-administrador', form, (response) => {
          if (response.error) {
            if(response.type == 'email_ja_cadastrado'){
              this.mensagemJaCadastrado();
              return console.log('erro');
            }
            return;
          }
          this.mensagemCriou('Administrador');
          this.router.navigate(['/manager/administrators']);
        });
      }else{
        this.mensagemSemPermissao();
      }
    }else if(this.novo_usuario == '2'){
      if(this.tipo_do_usuario == 1){
        this.padroes.requestPOST('/administrador/inserir-consultor', form, (response) => {
          if (response.error) {
            if(response.type == 'email_ja_cadastrado'){
              this.mensagemJaCadastrado();
              return console.log('erro');
            }
            return;
          }
          this.mensagemCriou('Consultor');
          this.router.navigate(['/manager/consultants']);
        });
      }else if(this.tipo_do_usuario == 4){
        this.padroes.requestPOST('/franqueado/inserir-consultor', form, (response) => {
          if (response.error) {
            if(response.type == 'email_ja_cadastrado'){
              this.mensagemJaCadastrado();
              return console.log('erro');
            }
            return;
          }
          this.mensagemCriou('Consultor');
          this.router.navigate(['/franchisee/consultants']);
        });
      }else{
          this.mensagemSemPermissao()
      }
    }else if(this.novo_usuario == '3'){
      if(this.tipo_do_usuario == 1){
        this.padroes.requestPOST('/administrador/inserir-cliente', form, (response) => {
          if (response.error) {
            if(response.type == 'email_ja_cadastrado'){
              this.mensagemJaCadastrado();
              return console.log('erro');
            }
            return;
          }
          this.mensagemCriou('Cliente');
          this.router.navigate(['/manager/customers']);
        });
      }else if(this.tipo_do_usuario == 2){
        this.padroes.requestPOST('/consultor/inserir-cliente', form, (response) => {
          if (response.error) {
            if(response.type == 'email_ja_cadastrado'){
              this.mensagemJaCadastrado();
              return console.log('erro');
            }
            return;
          }
          this.mensagemCriou('Cliente');
          this.router.navigate(['/consultant/clients']);
        });
      }else if(this.tipo_do_usuario == 4){
        this.padroes.requestPOST('/franqueado/inserir-cliente', form, (response) => {
          if (response.error) {
            if(response.type == 'email_ja_cadastrado'){
              this.mensagemJaCadastrado();
              return console.log('erro');
            }
            return;
          }
          this.mensagemCriou('Cliente');
          this.router.navigate(['/franchisee/clients']);
        });
      }else{
        this.mensagemSemPermissao()
      }
    }else{
      if(this.tipo_do_usuario == 1){
        this.padroes.requestPOST('/administrador/inserir-franqueado', form, (response) => {
          if (response.error) {
            if(response.type == 'email_ja_cadastrado'){
              this.mensagemJaCadastrado();
              return console.log('erro');
            }
            return;
          }
          this.mensagemCriou('Cliente');
          this.router.navigate(['/manager/customers']);
        });
      }else{
        this.mensagemSemPermissao();
      }
    }
  }

  voltar(){
    if(this.tipo_do_usuario == 1){
      if(this.novo_usuario == '1'){
        this.router.navigate(['/manager/administrators']);
      }else if(this.novo_usuario == '2'){
        this.router.navigate(['/manager/consultants']);
      }else if(this.novo_usuario == '3'){
        this.router.navigate(['/manager/customers']);
      }else{
        this.router.navigate(['/manager/franchisee']);
      }
    }else if(this.tipo_do_usuario == 2){
      this.router.navigate(['/consultant/clients']);
    }else{
      if(this.novo_usuario == '2'){
        this.router.navigate(['/franchisee/consultants']);
      }else{
        this.router.navigate(['/franchisee/customers']);
      }
    }
  }


}
