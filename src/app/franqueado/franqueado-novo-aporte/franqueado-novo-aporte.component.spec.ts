import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoNovoAporteComponent } from './franqueado-novo-aporte.component';

describe('FranqueadoNovoAporteComponent', () => {
  let component: FranqueadoNovoAporteComponent;
  let fixture: ComponentFixture<FranqueadoNovoAporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoNovoAporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoNovoAporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
