import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-franqueado-novo-aporte',
  templateUrl: './franqueado-novo-aporte.component.html',
  styleUrls: ['./franqueado-novo-aporte.component.css']
})
export class FranqueadoNovoAporteComponent implements OnInit {
  
  clientes: Array<any> = [];
  consultores: Array<any> = [];
  
  consultor_do_cliente = '';
  
  usuario;
  
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.padroes.requestGET('/franqueado/novo-aporte', (response) => {
      if (response.error) {
        return;
      }
      console.log(response);
      this.clientes = response.clientes;
      this.consultores = response.consultores;
    });
  }
  
  trocou_cliente(cliente){
    this.clientes.map((item) => {
      if(item.id == cliente){
        this.consultor_do_cliente = item.cliente_consultor;
        console.log(this.consultor_do_cliente);
      }
    });
    console.log(cliente);
    console.log(this.clientes);
  }
  
  mensagemCriou() {
    this.messageService.add({severity:'success', summary: 'Aporte criado', detail: 'com sucesso'});
  }
  
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
  }
  
  criar_aporte(form){
    this.padroes.requestPOST('/franqueado/inserir-aporte', form, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return console.log('erro');
      }
      this.mensagemCriou();
      this.router.navigate(['/franchisee/new-applications']);
    });
  }

}
