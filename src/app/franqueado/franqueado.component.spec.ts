import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoComponent } from './franqueado.component';

describe('FranqueadoComponent', () => {
  let component: FranqueadoComponent;
  let fixture: ComponentFixture<FranqueadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
