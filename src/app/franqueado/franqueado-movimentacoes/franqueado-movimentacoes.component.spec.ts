import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoMovimentacoesComponent } from './franqueado-movimentacoes.component';

describe('FranqueadoMovimentacoesComponent', () => {
  let component: FranqueadoMovimentacoesComponent;
  let fixture: ComponentFixture<FranqueadoMovimentacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoMovimentacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoMovimentacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
