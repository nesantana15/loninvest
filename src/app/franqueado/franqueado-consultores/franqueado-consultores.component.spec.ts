import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoConsultoresComponent } from './franqueado-consultores.component';

describe('FranqueadoConsultoresComponent', () => {
  let component: FranqueadoConsultoresComponent;
  let fixture: ComponentFixture<FranqueadoConsultoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoConsultoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoConsultoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
