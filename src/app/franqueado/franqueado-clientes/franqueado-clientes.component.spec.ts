import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoClientesComponent } from './franqueado-clientes.component';

describe('FranqueadoClientesComponent', () => {
  let component: FranqueadoClientesComponent;
  let fixture: ComponentFixture<FranqueadoClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
