import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-franqueado-clientes',
  templateUrl: './franqueado-clientes.component.html',
  styleUrls: ['./franqueado-clientes.component.css']
})
export class FranqueadoClientesComponent implements OnInit {
  
  index: number = -1;
  aportes: Array<any> = [];
  
  ordens: SelectItem[];
  ordem;
  busca;
  
  total_rendimentos = 0;
  total_aportes = 0;
  total_clientes = 0;
  
  clientes;
  urlContrato;
  
  ngOnInit() {
    this.urlContrato = this.padroes.urlContrato;
    this.padroes.requestGET('/franqueado/clientes', (response) => {
      if (response.error) {
        return;
      }
      this.clientes = response;
      this.clientes.map((item) => {
        this.total_clientes++;
        this.total_aportes = this.total_aportes + item.valor_aportes;
        this.total_rendimentos = this.total_rendimentos + item.valor_rendimentos;
        switch (item.genero) {
          case 'masculino':
            item.genero = (this.usuario.pais == 1 ? 'Male' : 'Masculino');
            break;
          case 'feminino':
            item.genero = (this.usuario.pais == 1 ? 'Female' : 'Feminino');
            break;
          case 'homosexual':
            item.genero = 'Homosexual';
            break;
          case 'transsexual':
            item.genero = 'Transsexual';
            break;
          case 'outro':
            item.genero = (this.usuario.pais == 1 ? 'Other' : 'Outro');
            break;
        }
        switch (item.estado_civil) {
          case 'casado':
            item.estado_civil = (this.usuario.pais == 1 ? 'Married' : 'Casado');
            break;
          case 'solteiro':
            item.estado_civil = (this.usuario.pais == 1 ? 'Unmarried' : 'Solteiro');
            break;
          case 'divorciado':
            item.estado_civil = (this.usuario.pais == 1 ? 'Divorced' : 'Divorciado');
            break;
          case 'viuvo':
            item.estado_civil = (this.usuario.pais == 1 ? 'Widower' : 'Viúvo');
            break;
          case 'outro':
            item.estado_civil = (this.usuario.pais == 1 ? 'Other' : 'Outro');
            break;
        }
        switch (item.moeda) {
          case 1:
            item.moeda = 'Libra';
            break;
          case 2:
            item.moeda = 'Real';
            break;
        }
        switch (item.pais) {
          case 1:
            item.pais = (this.usuario.pais == 1 ? 'England' : 'Inglaterra');
            break;
          case 2:
            item.pais = (this.usuario.pais == 1 ? 'Brazil' : 'Brasil');
            break;
        }
      });
      console.log(response);
    });
  }
  mensagemContratoInserido() {
    this.messageService.add({severity:'success', summary: 'Contrato inserido', detail: 'com sucesso'});
  }
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  upload(file, id, index, form) {
    
    let formData: FormData = new FormData();
    let files = file.files[0];
    
    formData.append('arquivo', files, files.name);
    
    this.padroes.requestPOST('/franqueado/inserir-contrato-cliente/?cliente_id='+id, formData, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemContratoInserido();
      this.clientes[index].contratos = response;
      console.log(response);
      form.clear();
    });
  }
  
  usuario: any = {};
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    this.usuario = this.padroes.verUsuario().usuario;
    this.ordens = [
      {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
      {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
    ];
  }
  
  input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
  input_busca_style_abrir = {'display': 'inline-block'};
  fechar_busca_style = {'display': 'none'};
  abrir_busca(){
    this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
    this.input_busca_style_abrir = {'display': 'none'};
    this.fechar_busca_style = {'display': 'block'};
  }
  fechar_busca(){
    this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    this.fechar_busca_style = {'display': 'none'};
    this.input_busca_style_abrir = {'display': 'inline-block'};
  }
  
  ordena() {
    this.padroes.requestGET('/franqueado/clientes?' + this.ordem.tipo + '=' + this.ordem.name, (response) => {
      if (response.error) {
        return;
      }
      this.clientes = response;
      
      console.log(this.clientes);
    });
  }
  
  buscar() {
    this.padroes.requestGET('/franqueado/clientes?busca=' + this.busca, (response) => {
      if (response.error) {
        return;
      }
      this.clientes = response;
      
      console.log(this.clientes);
    });
  }

}
