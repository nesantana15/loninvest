import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoInicialComponent } from './franqueado-inicial.component';

describe('FranqueadoInicialComponent', () => {
  let component: FranqueadoInicialComponent;
  let fixture: ComponentFixture<FranqueadoInicialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoInicialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoInicialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
