import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoRendimentosComponent } from './franqueado-rendimentos.component';

describe('FranqueadoRendimentosComponent', () => {
  let component: FranqueadoRendimentosComponent;
  let fixture: ComponentFixture<FranqueadoRendimentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoRendimentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoRendimentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
