import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-franqueado-rendimentos',
  templateUrl: './franqueado-rendimentos.component.html',
  styleUrls: ['./franqueado-rendimentos.component.css']
})
export class FranqueadoRendimentosComponent implements OnInit {
  
  index: number = -1;
  rendimentos: Array<any> = [];
  busca;
  
  usuario;
  
  total_comissoes;
  total_saques;
  total_ganho;
  saques;
  now;
  liberado_sacar;
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
  }
  
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.padroes.requestGET('/franqueado/meus-rendimentos', (response) => {
      if (response.error) {
        return;
      }
      this.now = response.now;
      this.saques = response.saques;
    });
    this.padroes.requestGET('/me', (response) => {
      if (response.error) {
        return;
      }
      console.log(response);
      this.total_comissoes = response.franqueado_total_comissao;
      this.total_saques = response.franqueado_total_saque;
      this.total_ganho = response.franqueado_total_saque + response.franqueado_total_comissao;
      this.liberado_sacar = response.franqueado_liberado_saque;
    });
  }
  mensagemDepositoSolicitado() {
    this.messageService.add({severity:'success', summary: 'Saque solicitado', detail: 'com sucesso'});
  }
  mensagemBancoSalvo() {
    this.messageService.add({severity:'success', summary: 'Banco cadastrado', detail: 'com sucesso'});
  }
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  sacar(valor){
    this.padroes.requestPOST('/franqueado/solicitar-saque/', valor, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemDepositoSolicitado();
      this.saques.push(response.saque);
      
    });
  }
  
  salvar_banco(valor){
    this.padroes.requestPOST('/franqueado/alterar-dados-bancarios/', valor, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemBancoSalvo();
      this.usuario.banco = valor.banco;
      this.usuario.agencia = valor.agencia;
      this.usuario.conta_corrente = valor.conta_corrente;
    });
  }
  salvar_bitcoin(valor){
    this.padroes.requestPOST('/franqueado/alterar-conta-bitcoin/', valor, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemBancoSalvo();
      this.usuario.conta_bitcoin = valor.conta_bitcoin;
    });
  }

}
