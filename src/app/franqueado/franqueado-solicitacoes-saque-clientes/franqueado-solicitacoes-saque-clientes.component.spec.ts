import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoSolicitacoesSaqueClientesComponent } from './franqueado-solicitacoes-saque-clientes.component';

describe('FranqueadoSolicitacoesSaqueClientesComponent', () => {
  let component: FranqueadoSolicitacoesSaqueClientesComponent;
  let fixture: ComponentFixture<FranqueadoSolicitacoesSaqueClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoSolicitacoesSaqueClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoSolicitacoesSaqueClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
