import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoSolicitacoesSaqueConsultoresComponent } from './franqueado-solicitacoes-saque-consultores.component';

describe('FranqueadoSolicitacoesSaqueConsultoresComponent', () => {
  let component: FranqueadoSolicitacoesSaqueConsultoresComponent;
  let fixture: ComponentFixture<FranqueadoSolicitacoesSaqueConsultoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoSolicitacoesSaqueConsultoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoSolicitacoesSaqueConsultoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
