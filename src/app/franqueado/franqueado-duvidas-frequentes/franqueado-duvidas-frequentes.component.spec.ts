import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoDuvidasFrequentesComponent } from './franqueado-duvidas-frequentes.component';

describe('FranqueadoDuvidasFrequentesComponent', () => {
  let component: FranqueadoDuvidasFrequentesComponent;
  let fixture: ComponentFixture<FranqueadoDuvidasFrequentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoDuvidasFrequentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoDuvidasFrequentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
