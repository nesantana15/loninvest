import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-franqueado-duvidas-frequentes',
  templateUrl: './franqueado-duvidas-frequentes.component.html',
  styleUrls: ['./franqueado-duvidas-frequentes.component.css']
})
export class FranqueadoDuvidasFrequentesComponent implements OnInit {
  
  index: number = -1;
  duvidas: Array<any> = [];
  
  ordens: SelectItem[];
  ordem;
  busca;
  
  usuario;
  
  total_duvidas;
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    this.usuario = this.padroes.verUsuario().usuario;
    this.ordens = [
      {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
      {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
    ];
  }
  
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.padroes.requestGET('/franqueado/duvidas-frequentes', (response) => {
      if (response.error) {
        return;
      }
      this.duvidas = response;
      this.total_duvidas = this.duvidas.length;
    });
  }
  
  input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
  fechar_busca_style = {'display': 'none'};
  
  abrir_busca() {
    this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
    this.fechar_busca_style = {'display': 'block'};
  }
  
  fechar_busca() {
    this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    this.fechar_busca_style = {'display': 'none'};
  }
  
  ordena() {
    this.padroes.requestGET('/franqueado/duvidas-frequentes?' + this.ordem.tipo + '=' + this.ordem.name, (response) => {
      if (response.error) {
        return;
      }
      this.duvidas = response;
      
      console.log(this.duvidas);
    });
  }
  
  buscar() {
    this.padroes.requestGET('/franqueado/duvidas-frequentes?busca=' + this.busca, (response) => {
      if (response.error) {
        return;
      }
      this.duvidas = response;
      
      console.log(this.duvidas);
    });
  }

}
