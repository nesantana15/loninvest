import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranqueadoAportesComponent } from './franqueado-aportes.component';

describe('FranqueadoAportesComponent', () => {
  let component: FranqueadoAportesComponent;
  let fixture: ComponentFixture<FranqueadoAportesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranqueadoAportesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranqueadoAportesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
