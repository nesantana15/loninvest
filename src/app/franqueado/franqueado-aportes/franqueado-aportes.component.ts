import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-franqueado-aportes',
  templateUrl: './franqueado-aportes.component.html',
  styleUrls: ['./franqueado-aportes.component.css']
})
export class FranqueadoAportesComponent implements OnInit {
  
  current: any = '';
  index: number = -1;
  aportes: Array<any> = [];
  dataHoje: Date;
  
  images: any[];
  ordens: SelectItem[];
  ordem;
  busca;
  urlComprovante;
  
  total_rendimentos;
  total_aportes;
  total_numero;
  
  usuario: any = {};
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    this.usuario = this.padroes.verUsuario().usuario;
    this.ordens = [
      {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
      {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
      {label: (this.usuario.pais == 1 ? 'Hight value' : 'Maior Valor'), value: {id: 3, name: 'maior-valor', tipo: 'ordem'}},
      {label: (this.usuario.pais == 1 ? 'Down value' : 'Menor Valor'), value: {id: 4, name: 'menor-valor', tipo: 'ordem'}},
    ];
  }
  
  mensagemComprovanteInserido() {
    this.messageService.add({severity:'success', summary: 'Comprovante inserido', detail: 'com sucesso'});
  }
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  mensagemAporteRemovido() {
    this.messageService.add({severity:'success', summary: 'Aporte removido', detail: 'com sucesso'});
  }
  upload(file, id, index, form) {
    
    let formData: FormData = new FormData();
    let files = file.files[0];
    
    formData.append('arquivo', files, files.name);
    
    this.padroes.requestPOST('/franqueado/inserir-comprovante?aporte_id='+id, formData, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemComprovanteInserido();
      if(this.aportes[index].situacao == 1){
        this.aportes[index].situacao = 2
      }
      
      this.aportes[index].comprovantes = response;
      console.log(response);
      form.clear();
    });
  }
  ngOnInit() {
    this.urlComprovante = this.padroes.urlComprovante;
    this.padroes.requestGET('/franqueado/aportes', (response) => {
      if(response.error){
        return;
      }
      this.aportes = response;
      
      console.log(this.aportes);
    });
    this.padroes.requestGET('/me', (response) => {
      if(response.error){
        return;
      }
      this.total_rendimentos = response.valor_rendimentos;
      this.total_aportes = response.valor_aportes;
      this.total_numero = response.total_aportes_aprovados + response.total_aportes_sem_comprovante + response.total_aportes_em_analise;
      console.log(response);
    });
  }
  
  input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
  input_busca_style_abrir = {'display': 'inline-block'};
  fechar_busca_style = {'display': 'none'};
  abrir_busca(){
    this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
    this.input_busca_style_abrir = {'display': 'none'};
    this.fechar_busca_style = {'display': 'block'};
  }
  fechar_busca(){
    this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    this.fechar_busca_style = {'display': 'none'};
    this.input_busca_style_abrir = {'display': 'inline-block'};
  }
  ordena(){
    this.padroes.requestGET('/franqueado/aportes?'+this.ordem.tipo+'='+this.ordem.name, (response) => {
      if(response.error){
        return;
      }
      this.aportes = response;
      
      console.log(this.aportes);
    });
  }
  
  buscar(){
    this.padroes.requestGET('/franqueado/aportes?busca='+this.busca, (response) => {
      if(response.error){
        return;
      }
      this.aportes = response;
      
      console.log(this.aportes);
    });
  }
  
  
  apagar_aporte(id){
    this.padroes.requestGET('/franqueado/apagar-aporte/'+id, (response) => {
      if(response.error){
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemAporteRemovido();
      let novo_aporte = [];
      this.aportes.filter((aporte) =>{
        if(aporte.id != id){
          novo_aporte.push(aporte);
        }
      });
      this.aportes = novo_aporte;
    });
  }

}
