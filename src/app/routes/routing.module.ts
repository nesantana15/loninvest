import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {ClienteComponent} from '../cliente/cliente.component';
import {AdministradorComponent} from '../administrador/administrador.component';
import {ConsultorComponent} from '../consultor/consultor.component';
import {AdminAportesComponent} from '../administrador/admin-aportes/admin-aportes.component';
import {AdminNovosAportesComponent} from '../administrador/admin-novos-aportes/admin-novos-aportes.component';
import {ConsultNovoAporteComponent} from '../consultor/consult-novo-aporte/consult-novo-aporte.component';
import {AdminMovimentacoesComponent} from '../administrador/admin-movimentacoes/admin-movimentacoes.component';
import {AdminClientesComponent} from '../administrador/admin-clientes/admin-clientes.component';
import {AdminConsultoresComponent} from '../administrador/admin-consultores/admin-consultores.component';
import {AdminSaquesClientesComponent} from '../administrador/admin-saques-clientes/admin-saques-clientes.component';
import {AdminSaquesConsultoresComponent} from '../administrador/admin-saques-consultores/admin-saques-consultores.component';
import {AdminNovoAporteComponent} from '../administrador/admin-novo-aporte/admin-novo-aporte.component';
import {ClienteAportesComponent} from '../cliente/cliente-aportes/cliente-aportes.component';
import {ClienteInicioComponent} from '../cliente/cliente-inicio/cliente-inicio.component';
import {ClienteMovimentacoesComponent} from '../cliente/cliente-movimentacoes/cliente-movimentacoes.component';
import {AdministradorInicialComponent} from '../administrador/administrador-inicial/administrador-inicial.component';
import {ConsultorInicialComponent} from '../consultor/consultor-inicial/consultor-inicial.component';
import {ConsultorAportesComponent} from '../consultor/consultor-aportes/consultor-aportes.component';
import {ConsultorSolicitacoesSaqueComponent} from '../consultor/consultor-solicitacoes-saque/consultor-solicitacoes-saque.component';
import {ConsultorMovimentacoesComponent} from '../consultor/consultor-movimentacoes/consultor-movimentacoes.component';
import {ConsultorClientesComponent} from '../consultor/consultor-clientes/consultor-clientes.component';
import {ConsultorRendimentosComponent} from '../consultor/consultor-rendimentos/consultor-rendimentos.component';
import {CriarUsuarioComponent} from '../criar-usuario/criar-usuario.component';
import {AdminAdministradoresComponent} from '../administrador/admin-administradores/admin-administradores.component';
import {TrocarSenhaComponent} from '../trocar-senha/trocar-senha.component';
import {EditarUsuarioComponent} from '../editar-usuario/editar-usuario.component';
import {NovaDuvidasFrequentesComponent} from '../nova-duvidas-frequentes/nova-duvidas-frequentes.component';
import {EditarDuvidasFrequentesComponent} from '../editar-duvidas-frequentes/editar-duvidas-frequentes.component';
import {ClienteDuvidasFrequentesComponent} from '../cliente/cliente-duvidas-frequentes/cliente-duvidas-frequentes.component';
import {AdminDuvidasFrequentesComponent} from '../administrador/admin-duvidas-frequentes/admin-duvidas-frequentes.component';
import {ConsultorDuvidasFrequentesComponent} from '../consultor/consultor-duvidas-frequentes/consultor-duvidas-frequentes.component';
import {ClienteContatoComponent} from '../cliente/cliente-contato/cliente-contato.component';
import {ResponderContatoComponent} from '../responder-contato/responder-contato.component';
import {CriarContatoComponent} from '../criar-contato/criar-contato.component';
import {FranqueadoComponent} from '../franqueado/franqueado.component';
import {FranqueadoInicialComponent} from '../franqueado/franqueado-inicial/franqueado-inicial.component';
import {FranqueadoAportesComponent} from '../franqueado/franqueado-aportes/franqueado-aportes.component';
import {FranqueadoSolicitacoesSaqueClientesComponent} from '../franqueado/franqueado-solicitacoes-saque-clientes/franqueado-solicitacoes-saque-clientes.component';
import {FranqueadoSolicitacoesSaqueConsultoresComponent} from '../franqueado/franqueado-solicitacoes-saque-consultores/franqueado-solicitacoes-saque-consultores.component';
import {FranqueadoMovimentacoesComponent} from '../franqueado/franqueado-movimentacoes/franqueado-movimentacoes.component';
import {FranqueadoDuvidasFrequentesComponent} from '../franqueado/franqueado-duvidas-frequentes/franqueado-duvidas-frequentes.component';
import {FranqueadoRendimentosComponent} from '../franqueado/franqueado-rendimentos/franqueado-rendimentos.component';
import {FranqueadoClientesComponent} from '../franqueado/franqueado-clientes/franqueado-clientes.component';
import {AdminFranqueadosComponent} from '../administrador/admin-franqueados/admin-franqueados.component';
import {AdminSaquesFranqueadosComponent} from '../administrador/admin-saques-franqueados/admin-saques-franqueados.component';
import {FranqueadoNovoAporteComponent} from '../franqueado/franqueado-novo-aporte/franqueado-novo-aporte.component';
import {FranqueadoConsultoresComponent} from '../franqueado/franqueado-consultores/franqueado-consultores.component';
import {EsqueciSenhaComponent} from '../esqueci-senha/esqueci-senha.component';

const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'client', component: ClienteComponent, children: [
        {path: '', redirectTo: 'initial', pathMatch: 'full'},
        {path: 'initial', component: ClienteInicioComponent},
        {path: 'applications', component: ClienteAportesComponent},
        {path: 'extract', component: ClienteMovimentacoesComponent} ,
        {path: 'questions', component: ClienteDuvidasFrequentesComponent},
        {path: 'contact', component: ClienteContatoComponent},
    ]},
    {path: 'manager', component: AdministradorComponent, children: [
        {path: '', redirectTo: 'initial', pathMatch: 'full'},
        {path: 'initial', component: AdministradorInicialComponent},
        {path: 'applications', component: AdminAportesComponent},
        {path: 'new-applications', component: AdminNovosAportesComponent},
        {path: 'news-withdrawal-clients', component: AdminSaquesClientesComponent},
        {path: 'news-withdrawal-consultants', component: AdminSaquesConsultoresComponent} ,
        {path: 'news-withdrawal-franchisee', component: AdminSaquesFranqueadosComponent} ,
        {path: 'extract', component: AdminMovimentacoesComponent},
        {path: 'customers', component: AdminClientesComponent},
        {path: 'consultants', component: AdminConsultoresComponent},
        {path: 'franchisee', component: AdminFranqueadosComponent},
        {path: 'administrators', component: AdminAdministradoresComponent},
        {path: 'questions', component: AdminDuvidasFrequentesComponent},
    ]},
    {path: 'consultant', component: ConsultorComponent, children: [
        {path: '', redirectTo: 'initial', pathMatch: 'full'},
        {path: 'initial', component: ConsultorInicialComponent},
        {path: 'applications', component: ConsultorAportesComponent},
        {path: 'news-withdrawal', component: ConsultorSolicitacoesSaqueComponent},
        {path: 'extract', component: ConsultorMovimentacoesComponent} ,
        {path: 'clients', component: ConsultorClientesComponent},
        {path: 'withdrawal', component: ConsultorRendimentosComponent},
        {path: 'questions', component: ConsultorDuvidasFrequentesComponent},
    ]},
    {path: 'franchisee', component: FranqueadoComponent, children: [
        {path: '', redirectTo: 'initial', pathMatch: 'full'},
        {path: 'initial', component: FranqueadoInicialComponent},
        {path: 'applications', component: FranqueadoAportesComponent},
        {path: 'news-withdrawal-costumers', component: FranqueadoSolicitacoesSaqueClientesComponent},
        {path: 'news-withdrawal-consultants', component: FranqueadoSolicitacoesSaqueConsultoresComponent},
        {path: 'extract', component: FranqueadoMovimentacoesComponent} ,
        {path: 'costumers', component: FranqueadoClientesComponent},
        {path: 'consultants', component: FranqueadoConsultoresComponent},
        {path: 'withdrawal', component: FranqueadoRendimentosComponent},
        {path: 'questions', component: FranqueadoDuvidasFrequentesComponent},
    ]},
    {path: 'manager-create-application', component: AdminNovoAporteComponent},
    {path: 'consultant-create-application', component: ConsultNovoAporteComponent},
    {path: 'franchisee-create-application', component: FranqueadoNovoAporteComponent},
    {path: 'create-user/:novo_usuario', component: CriarUsuarioComponent},
    {path: 'user-edit/:tipo/:usuario', component: EditarUsuarioComponent},
    {path: 'change-password', component: TrocarSenhaComponent},
    {path: 'create-questions', component: NovaDuvidasFrequentesComponent},
    {path: 'edit-questions/:id_duvida', component: EditarDuvidasFrequentesComponent},
    {path: 'reply-contact/:id_contato', component: ResponderContatoComponent},
    {path: 'create-contact', component: CriarContatoComponent},
    {path: 'forgot-password', component: EsqueciSenhaComponent},
    {path: '**', redirectTo: 'login'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class RoutingModule {}
