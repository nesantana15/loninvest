import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
    selector: 'app-editar-usuario',
    templateUrl: './editar-usuario.component.html',
    styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

    constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService, private route: ActivatedRoute) { }

    consultores;
    franqueados;
    tipo_do_usuario;

    id_usuario;
    tipo_usuario_editar;
    usuario_editar: any = {};

    usuario;
    
    ngOnInit() {
        this.usuario = this.padroes.verUsuario().usuario;

        this.route.params.subscribe(params => {
            this.id_usuario = params['usuario'];
            this.tipo_usuario_editar = params['tipo'];
        });

        let usuario = this.padroes.verUsuario().usuario;
        this.tipo_do_usuario = usuario.tipo;

        if(this.tipo_usuario_editar == 3){
            this.padroes.requestGET('/administrador/editar-dados-cliente?cliente_id='+this.id_usuario, (response) => {
                if (response.error) {
                    return;
                }
                this.usuario_editar = response;
                console.log(response);
            });
        }else if(this.tipo_usuario_editar == 2){
            this.padroes.requestGET('/administrador/editar-dados-consultor?consultor_id='+this.id_usuario, (response) => {
                if (response.error) {
                    return;
                }
                this.usuario_editar = response;
                console.log(response);
            });
        }else if(this.tipo_usuario_editar == 4){
            this.padroes.requestGET('/administrador/editar-dados-franqueado?franqueado_id='+this.id_usuario, (response) => {
                if (response.error) {
                    return;
                }
                this.usuario_editar = response;
                console.log(response);
            });
        }else{
            this.padroes.requestGET('/administrador/editar-dados-administrador?administrador_id='+this.id_usuario, (response) => {
                if (response.error) {
                    return;
                }
                this.usuario_editar = response;
                console.log(response);
            });
        }

        if(this.tipo_do_usuario == 1) {
            this.padroes.requestGET('/administrador/novo-cliente', (response) => {
                if (response.error) {
                    return;
                }
                this.consultores = response.consultores;
                this.franqueados = response.franqueados;
            });
        }

    }


    mensagemCriou(text) {
        this.messageService.add({severity:'success', summary: text, detail: 'editado com sucesso'});
    }

    mensagemJaCadastrado() {
        this.messageService.add({severity:'error', summary: 'Usuário', detail: 'ja cadastrado'});
    }

    mensagemSemPermissao() {
        this.messageService.add({severity:'error', summary: 'Sem permissão', detail: 'Você não tem permissão para isso.'});
    }

    enviar(form){
        if(this.usuario_editar.tipo == '1'){
            if(this.tipo_do_usuario == 1){
                this.padroes.requestPOST('/administrador/editar-administrador/'+this.usuario_editar.id, form, (response) => {
                    if (response.error) {
                        if(response.type == 'email_ja_cadastrado'){
                            this.mensagemJaCadastrado();
                            return console.log('erro');
                        }
                        return;
                    }
                    this.mensagemCriou('Administrador');
                    this.router.navigate(['/manager/administrators']);
                });
            }else{
                this.mensagemSemPermissao();
            }
        }else if(this.usuario_editar.tipo == '2'){
            if(this.tipo_do_usuario == 1){
                this.padroes.requestPOST('/administrador/editar-consultor/'+this.usuario_editar.id, form, (response) => {
                    if (response.error) {
                        if(response.type == 'email_ja_cadastrado'){
                            this.mensagemJaCadastrado();
                            return console.log('erro');
                        }
                        return;
                    }
                    this.mensagemCriou('Consultor');
                    this.router.navigate(['/manager/customers']);
                });
            }else{
                this.mensagemSemPermissao()
            }
        }else if(this.usuario_editar.tipo == '4'){
            if(this.tipo_do_usuario == 1){
                this.padroes.requestPOST('/administrador/editar-franqueado/'+this.usuario_editar.id, form, (response) => {
                    if (response.error) {
                        if(response.type == 'email_ja_cadastrado'){
                            this.mensagemJaCadastrado();
                            return console.log('erro');
                        }
                        return;
                    }
                    this.mensagemCriou('Cliente');
                    this.router.navigate(['/manager/customers']);
                });
            }
        }else{
            if(this.tipo_do_usuario == 1){
                this.padroes.requestPOST('/administrador/editar-cliente/'+this.usuario_editar.id, form, (response) => {
                    if (response.error) {
                        if(response.type == 'email_ja_cadastrado'){
                            this.mensagemJaCadastrado();
                            return console.log('erro');
                        }
                        return;
                    }
                    this.mensagemCriou('Cliente');
                    this.router.navigate(['/manager/customers']);
                });
            }
        }
    }

    voltar(){
        if(this.tipo_do_usuario == 1){
            if(this.tipo_usuario_editar == 1){
                this.router.navigate(['manager/administrators']);
            }else if(this.tipo_usuario_editar == 2){
                this.router.navigate(['manager/consultants']);
            }else if(this.tipo_usuario_editar == 4){
                this.router.navigate(['manager/franchisee']);
            }else{
                this.router.navigate(['manager/customers']);
            }
        }else if(this.tipo_do_usuario == 3) {
            if(this.tipo_usuario_editar == 2){
                this.router.navigate(['franchisee/consultants']);
            }else{
                this.router.navigate(['franchisee/customers']);
            }
        }else{
            this.router.navigate(['/consultant/clients']);
        }
    }
}
