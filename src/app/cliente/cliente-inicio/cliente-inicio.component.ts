import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-cliente-inicio',
  templateUrl: './cliente-inicio.component.html',
  styleUrls: ['./cliente-inicio.component.css']
})
export class ClienteInicioComponent implements OnInit {
    
    data: any;
    
    primeiro_valor = 0;
    ultimo_valor = 0;
    final_cotacao = 0;
    
    usd:number = null;
    uk:number = null;
    bt:number = null;
    
    deposito(valores){
        this.uk = Number(this.uk);
        this.usd = Number(this.usd);
        this.bt = Number(this.bt);
        valores.valor_dolar = Number(valores.valor_dolar);
        switch (valores.moeda) {
            case 'uk':
                this.final_cotacao = (valores.valor_dolar * this.usd) / (this.uk + (this.uk * 0.03));
                break;
            case 'br':
                this.final_cotacao = valores.valor_dolar * (this.usd + (this.usd * 0.03));
                break;
            case 'bt':
                this.final_cotacao = (valores.valor_dolar * this.usd) / (this.bt + (this.bt * 0.03));
                break;
        }
    }
    
    simular(valores){
        let valor_final = 0;
        valores.valor = Number(valores.valor);
        this.primeiro_valor = Number(valores.valor);
        valores.meses = Number(valores.meses);
        for(let cont_meses = 0; cont_meses < valores.meses; cont_meses++){
            if(cont_meses == 0){
                valor_final = valores.valor + (valores.valor * 0.1);
            }else{
                valor_final+= valor_final * 0.1;
            }
        }
        this.ultimo_valor = valor_final;
    }
    
    ngOnInit(){
        this.usuario = this.padores.verUsuario().usuario;
        
        this.http.get('https://economia.awesomeapi.com.br/all')
            .pipe(map(data => data || []))
            .subscribe((response: any) => {
                if(response.error){
                    if(response.auth){
                        console.log(response);
                        return
                    }
                    console.log(response);
                    return
                }
                this.usd = response.USD.high;
                this.uk = response.EUR.high;
                this.bt = response.BTC.high;
                console.log(this.usd, this.uk, this.bt);
                return
            });
        
    }
    
    usuario;
    
    constructor(private messageService: MessageService, private padores: PadroesService, private http: HttpClient) {}

}
