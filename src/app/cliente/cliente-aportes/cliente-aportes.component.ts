import {Component, OnInit} from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-cliente-aportes',
    templateUrl: './cliente-aportes.component.html',
    styleUrls: ['./cliente-aportes.component.css']
})
export class ClienteAportesComponent implements OnInit {

    index: number = -1;
    aportes: Array<any> = [];

    ordens: SelectItem[];
    ordem;
    busca;

    usuario;
    
    total_rendimentos;
    total_aportes;
    total_numero;

    constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
        this.usuario = this.padroes.verUsuario().usuario;
        this.ordens = [
            {label: (this.usuario.pais == 1 ? 'Last created' : 'Últimos Cadastros'), value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
            {label: (this.usuario.pais == 1 ? 'First created' : 'Primeiros Cadastros'), value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
            {label: (this.usuario.pais == 1 ? 'Hight value' : 'Maior Valor'), value: {id: 3, name: 'maior-valor', tipo: 'ordem'}},
            {label: (this.usuario.pais == 1 ? 'Down value' : 'Menor Valor'), value: {id: 4, name: 'menor-valor', tipo: 'ordem'}},
        ];
    }
    
    ngOnInit() {
        this.usuario = this.padroes.verUsuario().usuario;
        this.padroes.requestGET('/cliente/aportes', (response) => {
            if (response.error) {
                return;
            }
            this.aportes = response;
            console.log('teste');
            console.log(this.aportes);
        });
        this.padroes.requestGET('/me', (response) => {
            if (response.error) {
                return;
            }
            this.total_rendimentos = response.valor_rendimentos;
            this.total_aportes = response.valor_aportes;
            this.total_numero = response.total_aportes_aprovados;
            this.usuario = response;
        });
    }
    
    input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    input_busca_style_abrir = {'display': 'inline-block'};
    fechar_busca_style = {'display': 'none'};
    abrir_busca(){
        this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
        this.input_busca_style_abrir = {'display': 'none'};
        this.fechar_busca_style = {'display': 'block'};
    }
    fechar_busca(){
        this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
        this.fechar_busca_style = {'display': 'none'};
        this.input_busca_style_abrir = {'display': 'inline-block'};
    }

    ordena() {
        this.padroes.requestGET('/cliente/aportes?' + this.ordem.tipo + '=' + this.ordem.name, (response) => {
            if (response.error) {
                return;
            }
            this.aportes = response;

            console.log(this.aportes);
        });
    }

    buscar() {
        this.padroes.requestGET('/cliente/aportes?busca=' + this.busca, (response) => {
            if (response.error) {
                return;
            }
            this.aportes = response;

            console.log(this.aportes);
        });
    }
    
    mensagemDepositoSolicitado() {
        this.messageService.add({severity:'success', summary: 'Saque solicitado', detail: 'com sucesso'});
    }
    mensagemBancoSalvo() {
        this.messageService.add({severity:'success', summary: 'Banco salvo', detail: 'com sucesso'});
    }
    mensagemErroInesperado() {
        this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
    }
    
    sacar(valor, aporte_id, index){
        this.padroes.requestPOST('/cliente/solicitar-saque/'+aporte_id, valor, (response) => {
            if (response.error) {
                this.mensagemErroInesperado();
                return;
            }
            this.mensagemDepositoSolicitado();
            this.aportes[index].solicitacao_saque = true;
            this.aportes[index].periodo_sacado = true;
        });
    }
    
    salvar_banco(valor){
        this.padroes.requestPOST('/cliente/alterar-dados-bancarios/', valor, (response) => {
            if (response.error) {
                this.mensagemErroInesperado();
                return;
            }
            this.mensagemBancoSalvo();
            this.usuario.banco = valor.banco;
            this.usuario.agencia = valor.agencia;
            this.usuario.conta_corrente = valor.conta_corrente;
        });
    }
    salvar_bitcoin(valor){
        this.padroes.requestPOST('/cliente/alterar-conta-bitcoin/', valor, (response) => {
            if (response.error) {
                this.mensagemErroInesperado();
                return;
            }
            this.mensagemBancoSalvo();
            this.usuario.conta_bitcoin = valor.conta_bitcoin;
        });
    }


}
