import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteAportesComponent } from './cliente-aportes.component';

describe('ClienteAportesComponent', () => {
  let component: ClienteAportesComponent;
  let fixture: ComponentFixture<ClienteAportesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteAportesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteAportesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
