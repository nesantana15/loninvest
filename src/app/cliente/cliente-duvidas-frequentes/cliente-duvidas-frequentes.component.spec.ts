import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteDuvidasFrequentesComponent } from './cliente-duvidas-frequentes.component';

describe('ClienteDuvidasFrequentesComponent', () => {
  let component: ClienteDuvidasFrequentesComponent;
  let fixture: ComponentFixture<ClienteDuvidasFrequentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteDuvidasFrequentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteDuvidasFrequentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
