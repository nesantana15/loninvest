import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cliente-contato',
  templateUrl: './cliente-contato.component.html',
  styleUrls: ['./cliente-contato.component.css']
})
export class ClienteContatoComponent implements OnInit {
  
  index: number = -1;
  contatos: Array<any> = [];
  
  ordens: SelectItem[];
  ordem;
  busca;
  
  usuario;
  
  total_contatos;
  total_contatos_respondidos = 0;
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    this.ordens = [
      {label: 'Últimos Cadastros', value: {id: 1, name: 'ultimos', tipo: 'ordem'}},
      {label: 'Primeiros Cadastros', value: {id: 2, name: 'primeiros', tipo: 'ordem'}},
    ];
  }
  
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.padroes.requestGET('/cliente/contato', (response) => {
      if (response.error) {
        return;
      }
      this.contatos = response;
      this.contatos.map((contato) => {
        if(contato.respostas.length){
          this.total_contatos_respondidos++;
        }
      });
      this.total_contatos = this.contatos.length;
    });
  }
  
  input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
  fechar_busca_style = {'display': 'none'};
  
  abrir_busca() {
    this.input_busca_style = {'width': '200px', 'opacity': '1', 'margin-left': '10px'};
    this.fechar_busca_style = {'display': 'block'};
  }
  
  fechar_busca() {
    this.input_busca_style = {'width': '0', 'opacity': '0', 'margin-left': '0'};
    this.fechar_busca_style = {'display': 'none'};
  }
  
  ordena() {
    this.padroes.requestGET('/cliente/contato?' + this.ordem.tipo + '=' + this.ordem.name, (response) => {
      if (response.error) {
        return;
      }
      this.contatos = response;
      
      console.log(this.contatos);
    });
  }
  
  buscar() {
    this.padroes.requestGET('/cliente/contato?busca=' + this.busca, (response) => {
      if (response.error) {
        return;
      }
      this.contatos = response;
      
      console.log(this.contatos);
    });
  }

}
