import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteMovimentacoesComponent } from './cliente-movimentacoes.component';

describe('ClienteMovimentacoesComponent', () => {
  let component: ClienteMovimentacoesComponent;
  let fixture: ComponentFixture<ClienteMovimentacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteMovimentacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteMovimentacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
