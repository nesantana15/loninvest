import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  
  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) { }
  
  usuario: any = {};
  tipo_cliente;



    mensagemSaiu() {
        this.messageService.add({severity:'success', summary: 'Usuário saiu', detail: 'com sucesso'});
    }

    mensagemErroInesperado() {
        this.messageService.add({severity:'error', summary: 'Opaa, aconteceu um erro', detail: 'ao tentar sair, mas já estamos arrumando.'});
    }

    sair(){
        this.padroes.requestGET('/logout', (response) => {
            if (response.error) {
                this.mensagemErroInesperado();
                return;
            }
            this.mensagemSaiu();
            this.padroes.setarUsuario(null);
            this.router.navigate(['/']);
        });
    }
  
  ngOnInit() {
    this.padroes.requestGET('/me', (response) => {
      this.usuario = response;
      if(this.usuario.tipo == 1){
        this.router.navigate(['/manager']);
      }else if(this.usuario.tipo == 2){
        this.router.navigate(['/consultant']);
      }else if(this.usuario.tipo == 3){
            this.tipo_cliente = 'Cliente';
      }else{
          this.router.navigate(['/franchisee']);
          this.tipo_cliente = 'Franqueado'
      }
    });
  }

}
