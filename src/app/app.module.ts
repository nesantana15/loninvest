import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { DatePipe } from '@angular/common'

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AccordionModule} from 'primeng/accordion';
import {CalendarModule} from 'primeng/calendar';
import {DropdownModule} from 'primeng/dropdown';
import {FileUploadModule} from 'primeng/fileupload';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {LightboxModule} from 'primeng/lightbox';
import {InputMaskModule} from 'primeng/inputmask';
import {ToastModule} from 'primeng/toast';
import {ChartModule} from 'primeng/chart';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {MenuItem} from 'primeng/api';

import {AppComponent} from './app.component';
import {RoutingModule} from './routes/routing.module';
import {AdministradorComponent} from './administrador/administrador.component';
import {ClienteComponent} from './cliente/cliente.component';
import {ConsultorComponent} from './consultor/consultor.component';
import {LoginComponent} from './login/login.component';
import {AdminAportesComponent} from './administrador/admin-aportes/admin-aportes.component';
import {AdminNovosAportesComponent} from './administrador/admin-novos-aportes/admin-novos-aportes.component';
import {AdminMovimentacoesComponent} from './administrador/admin-movimentacoes/admin-movimentacoes.component';
import {AdminClientesComponent} from './administrador/admin-clientes/admin-clientes.component';
import {AdminConsultoresComponent} from './administrador/admin-consultores/admin-consultores.component';
import {AdminAdministradoresComponent} from './administrador/admin-administradores/admin-administradores.component';
import {AdminSaquesClientesComponent} from './administrador/admin-saques-clientes/admin-saques-clientes.component';
import {AdminSaquesConsultoresComponent} from './administrador/admin-saques-consultores/admin-saques-consultores.component';
import { AdminNovoAporteComponent } from './administrador/admin-novo-aporte/admin-novo-aporte.component';
import { ConsultNovoAporteComponent } from './consultor/consult-novo-aporte/consult-novo-aporte.component';
import { ClienteInicioComponent } from './cliente/cliente-inicio/cliente-inicio.component';
import { ClienteAportesComponent } from './cliente/cliente-aportes/cliente-aportes.component';
import { ClienteMovimentacoesComponent } from './cliente/cliente-movimentacoes/cliente-movimentacoes.component';
import { ConsultorAportesComponent } from './consultor/consultor-aportes/consultor-aportes.component';
import { ConsultorSolicitacoesSaqueComponent } from './consultor/consultor-solicitacoes-saque/consultor-solicitacoes-saque.component';
import { ConsultorMovimentacoesComponent } from './consultor/consultor-movimentacoes/consultor-movimentacoes.component';
import { ConsultorClientesComponent } from './consultor/consultor-clientes/consultor-clientes.component';
import { ConsultorRendimentosComponent } from './consultor/consultor-rendimentos/consultor-rendimentos.component';
import { AdministradorInicialComponent } from './administrador/administrador-inicial/administrador-inicial.component';
import { ConsultorInicialComponent } from './consultor/consultor-inicial/consultor-inicial.component';
import { CriarUsuarioComponent } from './criar-usuario/criar-usuario.component';
import { TrocarSenhaComponent } from './trocar-senha/trocar-senha.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { NovaDuvidasFrequentesComponent } from './nova-duvidas-frequentes/nova-duvidas-frequentes.component';
import { EditarDuvidasFrequentesComponent } from './editar-duvidas-frequentes/editar-duvidas-frequentes.component';
import { AdminDuvidasFrequentesComponent } from './administrador/admin-duvidas-frequentes/admin-duvidas-frequentes.component';
import { ConsultorDuvidasFrequentesComponent } from './consultor/consultor-duvidas-frequentes/consultor-duvidas-frequentes.component';
import { ClienteDuvidasFrequentesComponent } from './cliente/cliente-duvidas-frequentes/cliente-duvidas-frequentes.component';
import {AdminContatoComponent} from './administrador/admin-contato/admin-contato.component';
import {ConsultorContatoComponent} from './consultor/consultor-contato/consultor-contato.component';
import { ResponderContatoComponent } from './responder-contato/responder-contato.component';
import { CriarContatoComponent } from './criar-contato/criar-contato.component';
import { ClienteContatoComponent } from './cliente/cliente-contato/cliente-contato.component';
import { FranqueadoComponent } from './franqueado/franqueado.component';
import { FranqueadoNovoAporteComponent } from './franqueado/franqueado-novo-aporte/franqueado-novo-aporte.component';
import { FranqueadoAportesComponent } from './franqueado/franqueado-aportes/franqueado-aportes.component';
import { FranqueadoClientesComponent } from './franqueado/franqueado-clientes/franqueado-clientes.component';
import { FranqueadoConsultoresComponent } from './franqueado/franqueado-consultores/franqueado-consultores.component';
import { FranqueadoInicialComponent } from './franqueado/franqueado-inicial/franqueado-inicial.component';
import { FranqueadoMovimentacoesComponent } from './franqueado/franqueado-movimentacoes/franqueado-movimentacoes.component';
import { FranqueadoRendimentosComponent } from './franqueado/franqueado-rendimentos/franqueado-rendimentos.component';
import { FranqueadoSolicitacoesSaqueClientesComponent } from './franqueado/franqueado-solicitacoes-saque-clientes/franqueado-solicitacoes-saque-clientes.component';
import { FranqueadoSolicitacoesSaqueConsultoresComponent } from './franqueado/franqueado-solicitacoes-saque-consultores/franqueado-solicitacoes-saque-consultores.component';
import { FranqueadoDuvidasFrequentesComponent } from './franqueado/franqueado-duvidas-frequentes/franqueado-duvidas-frequentes.component';
import { AdminFranqueadosComponent } from './administrador/admin-franqueados/admin-franqueados.component';
import { AdminSaquesFranqueadosComponent } from './administrador/admin-saques-franqueados/admin-saques-franqueados.component';
import { EsqueciSenhaComponent } from './esqueci-senha/esqueci-senha.component';

@NgModule({
    declarations: [
        AppComponent,
        AdministradorComponent,
        ClienteComponent,
        ConsultorComponent,
        LoginComponent,
        AdminAportesComponent,
        AdminNovosAportesComponent,
        AdminMovimentacoesComponent,
        AdminClientesComponent,
        AdminConsultoresComponent,
        AdminAdministradoresComponent,
        AdminSaquesClientesComponent,
        AdminSaquesConsultoresComponent,
        AdminNovoAporteComponent,
        ConsultNovoAporteComponent,
        ClienteInicioComponent,
        ClienteAportesComponent,
        ClienteMovimentacoesComponent,
        ConsultorAportesComponent,
        ConsultorSolicitacoesSaqueComponent,
        ConsultorMovimentacoesComponent,
        ConsultorClientesComponent,
        ConsultorRendimentosComponent,
        AdministradorInicialComponent,
        ConsultorInicialComponent,
        CriarUsuarioComponent,
        TrocarSenhaComponent,
        EditarUsuarioComponent,
        NovaDuvidasFrequentesComponent,
        EditarDuvidasFrequentesComponent,
        AdminDuvidasFrequentesComponent,
        ConsultorDuvidasFrequentesComponent,
        ClienteDuvidasFrequentesComponent,
        AdminContatoComponent,
        ConsultorContatoComponent,
        ResponderContatoComponent,
        CriarContatoComponent,
        ClienteContatoComponent,
        FranqueadoComponent,
        FranqueadoNovoAporteComponent,
        FranqueadoAportesComponent,
        FranqueadoClientesComponent,
        FranqueadoConsultoresComponent,
        FranqueadoInicialComponent,
        FranqueadoMovimentacoesComponent,
        FranqueadoRendimentosComponent,
        FranqueadoSolicitacoesSaqueClientesComponent,
        FranqueadoSolicitacoesSaqueConsultoresComponent,
        FranqueadoDuvidasFrequentesComponent,
        AdminFranqueadosComponent,
        AdminSaquesFranqueadosComponent,
        EsqueciSenhaComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        RoutingModule,
        AccordionModule,
        CalendarModule,
        DropdownModule,
        FileUploadModule,
        MessagesModule,
        MessageModule,
        LightboxModule,
        InputMaskModule,
        ToastModule,
        ChartModule,
        OverlayPanelModule
    ],
    providers: [DatePipe],
    bootstrap: [AppComponent]
})
export class AppModule {
}
