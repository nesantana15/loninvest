import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorSolicitacoesSaqueComponent } from './consultor-solicitacoes-saque.component';

describe('ConsultorSolicitacoesSaqueComponent', () => {
  let component: ConsultorSolicitacoesSaqueComponent;
  let fixture: ComponentFixture<ConsultorSolicitacoesSaqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultorSolicitacoesSaqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorSolicitacoesSaqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
