import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorContatoComponent } from './consultor-contato.component';

describe('ConsultorContatoComponent', () => {
  let component: ConsultorContatoComponent;
  let fixture: ComponentFixture<ConsultorContatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultorContatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorContatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
