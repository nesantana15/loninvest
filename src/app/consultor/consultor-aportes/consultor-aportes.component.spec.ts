import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorAportesComponent } from './consultor-aportes.component';

describe('ConsultorAportesComponent', () => {
  let component: ConsultorAportesComponent;
  let fixture: ComponentFixture<ConsultorAportesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultorAportesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorAportesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
