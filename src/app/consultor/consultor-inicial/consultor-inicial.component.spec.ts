import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorInicialComponent } from './consultor-inicial.component';

describe('ConsultorInicialComponent', () => {
  let component: ConsultorInicialComponent;
  let fixture: ComponentFixture<ConsultorInicialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultorInicialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorInicialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
