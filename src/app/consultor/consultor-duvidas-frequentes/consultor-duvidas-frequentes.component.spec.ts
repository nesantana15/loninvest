import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorDuvidasFrequentesComponent } from './consultor-duvidas-frequentes.component';

describe('ConsultorDuvidasFrequentesComponent', () => {
  let component: ConsultorDuvidasFrequentesComponent;
  let fixture: ComponentFixture<ConsultorDuvidasFrequentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultorDuvidasFrequentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorDuvidasFrequentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
