import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../funcoes/padroes.service';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-consultor',
  templateUrl: './consultor.component.html',
  styleUrls: ['./consultor.component.css']
})
export class ConsultorComponent implements OnInit {

  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
    setInterval(() => {
      console.log('teste');
      this.padroes.requestGET('/consultor/totais', (response) => {
	        this.aportes = response.total_aportes_nao_aprovados;
	        this.saques_cliente = response.total_solicitacoes_saque_cliente;
          this.contato = response.total_contatos_nao_respondidos;
      });
  }, 1000*30);
  }

  usuario: any = {};
  tipo_cliente;

  aportes;
  saques_cliente;
  contato;

  mensagemSaiu() {
      this.messageService.add({severity:'success', summary: 'Usuário saiu', detail: 'com sucesso'});
  }

  mensagemErroInesperado() {
      this.messageService.add({severity:'error', summary: 'Opaa, aconteceu um erro', detail: 'ao tentar sair, mas já estamos arrumando.'});
  }

  sair(){
    this.padroes.requestGET('/logout', (response) => {
        if (response.error) {
            this.mensagemErroInesperado();
            return;
        }
        this.mensagemSaiu();
        this.padroes.setarUsuario(null);
        this.router.navigate(['/']);
    });
  }
  
  ngOnInit() {
    this.padroes.requestGET('/me', (response) => {
      this.usuario = response;
      if(this.usuario.tipo == 1){
        this.router.navigate(['/manager']);
      }else if(this.usuario.tipo == 2){
        
        if(this.usuario.pais == 1){
          this.tipo_cliente = 'Consultant';
        }else{
          this.tipo_cliente = 'Consultor';
        }
        
        this.padroes.requestGET('/consultor/totais', (response) => {
	        this.aportes = response.total_aportes_nao_aprovados;
	        this.saques_cliente = response.total_solicitacoes_saque_cliente;
          this.contato = response.total_contatos_nao_respondidos;
        });
      }else if(this.usuario.tipo == 3){
        this.router.navigate(['/client']);
      }else{
        this.router.navigate(['/franchisee']);
      }
    });
  }

}
