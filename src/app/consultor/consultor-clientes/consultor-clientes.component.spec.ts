import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorClientesComponent } from './consultor-clientes.component';

describe('ConsultorClientesComponent', () => {
  let component: ConsultorClientesComponent;
  let fixture: ComponentFixture<ConsultorClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultorClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
