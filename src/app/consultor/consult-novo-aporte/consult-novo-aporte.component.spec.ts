import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultNovoAporteComponent } from './consult-novo-aporte.component';

describe('ConsultNovoAporteComponent', () => {
  let component: ConsultNovoAporteComponent;
  let fixture: ComponentFixture<ConsultNovoAporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultNovoAporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultNovoAporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
