import { Component, OnInit } from '@angular/core';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-consult-novo-aporte',
  templateUrl: './consult-novo-aporte.component.html',
  styleUrls: ['./consult-novo-aporte.component.css']
})
export class ConsultNovoAporteComponent implements OnInit {

  constructor(private padroes: PadroesService, private router: Router) {
  }

  clientes: Array<any> = [];
  consultores: Array<any> = [];

  usuario: any = {};
  
  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.padroes.requestGET('/consultor/novo-aporte', (response) => {
      if (response.error) {
        return;
      }
      this.clientes = response;
    });
  }

  criar_aporte(form){
    console.log(form);
    this.padroes.requestPOST('/consultor/inserir-aporte', form, (response) => {
      if (response.error) {
        return console.log('erro');
      }
      this.router.navigate(['/consultant/applications']);
    });
  }

}
