import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorMovimentacoesComponent } from './consultor-movimentacoes.component';

describe('ConsultorMovimentacoesComponent', () => {
  let component: ConsultorMovimentacoesComponent;
  let fixture: ComponentFixture<ConsultorMovimentacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultorMovimentacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorMovimentacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
