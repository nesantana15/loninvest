import { Component, OnInit } from '@angular/core';
import {MessageService, SelectItem} from 'primeng/api';
import {PadroesService} from '../../funcoes/padroes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-consultor-rendimentos',
  templateUrl: './consultor-rendimentos.component.html',
  styleUrls: ['./consultor-rendimentos.component.css']
})
export class ConsultorRendimentosComponent implements OnInit {

  index: number = -1;
  rendimentos: Array<any> = [];
  busca;
  
  usuario;

  total_comissoes;
  total_saques;
  total_ganho;
  saques;
  now;
  liberado_sacar;

  constructor(private padroes: PadroesService, private router: Router, private messageService: MessageService) {
  }

  ngOnInit() {
    this.usuario = this.padroes.verUsuario().usuario;
    this.padroes.requestGET('/consultor/meus-rendimentos', (response) => {
      if (response.error) {
        return;
      }
      this.now = response.now;
      this.saques = response.saques;
    });
    this.padroes.requestGET('/me', (response) => {
      if (response.error) {
        return;
      }
      console.log(response);
      this.total_comissoes = response.consultor_total_comissao;
      this.total_saques = response.consultor_total_saque;
      this.total_ganho = response.consultor_total_saque + response.consultor_total_comissao;
      this.liberado_sacar = response.consultor_liberado_saque;
    });
  }
  mensagemDepositoSolicitado() {
    this.messageService.add({severity:'success', summary: 'Saque solicitado', detail: 'com sucesso'});
  }
  mensagemBancoSalvo() {
    this.messageService.add({severity:'success', summary: 'Banco cadastrado', detail: 'com sucesso'});
  }
  mensagemErroInesperado() {
    this.messageService.add({severity:'error', summary: 'Opaa, deu um erro', detail: 'mas já estamos arrumando.'});
  }
  sacar(valor){
    this.padroes.requestPOST('/consultor/solicitar-saque/', valor, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemDepositoSolicitado();
      this.saques.push(response.saque);

    });
  }
  
  salvar_banco(valor){
    this.padroes.requestPOST('/consultor/alterar-dados-bancarios/', valor, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemBancoSalvo();
      this.usuario.banco = valor.banco;
      this.usuario.agencia = valor.agencia;
      this.usuario.conta_corrente = valor.conta_corrente;
    });
  }
  salvar_bitcoin(valor){
    this.padroes.requestPOST('/consultor/alterar-conta-bitcoin/', valor, (response) => {
      if (response.error) {
        this.mensagemErroInesperado();
        return;
      }
      this.mensagemBancoSalvo();
      this.usuario.conta_bitcoin = valor.conta_bitcoin;
    });
  }

}
