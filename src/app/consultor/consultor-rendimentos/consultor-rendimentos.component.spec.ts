import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorRedimentosComponent } from './consultor-redimentos.component';

describe('ConsultorRedimentosComponent', () => {
  let component: ConsultorRedimentosComponent;
  let fixture: ComponentFixture<ConsultorRedimentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultorRedimentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorRedimentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
